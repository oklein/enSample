#ifndef ENSAMPLE_HOOKS_HH
#define ENSAMPLE_HOOKS_HH

#include <memory>
#include <fstream>

#ifdef USE_GNUPLOT
#define GNUPLOT_DEPRECATE_WARN
#include "gnuplot-iostream.h"
#endif // USE_GNUPLOT

namespace enSample {

  /**
   * @brief Abstract base class for hook functions (callbacks)
   */
  template<typename Vector>
    class Hook
    {
      using Real = typename Vector::Real;

      public:

      virtual ~Hook() {}

      /**
       * @brief Name of hook
       */
      virtual std::string name() const = 0;

      /**
       * @brief Hook callback function
       *
       * @param x       vector of current MCMC chain states
       * @param log_pi  vector of corresponding log-density values
       * @param i_chain index of state that was treated during step
       */
      virtual void call(const std::vector<Vector>& x, const std::vector<Real>& log_pi,
          std::size_t i_chain) const = 0;
    };

  /**
   * @brief Small hook class that prints every state
   */
  template<typename Vector>
    class EchoHook : public Hook<Vector>
  {
    using Real = typename Vector::Real;

    public:

    std::string name() const override
    {
      return "echo";
    }

    void call(const std::vector<Vector>& x, const std::vector<Real>& log_pi,
        std::size_t i_chain) const override
    {
      const Vector& vec = x[i_chain];
      for (std::size_t i = 0; i < vec.dim() - 1; i++)
        std::cout << vec[i] << " ";
      std::cout << vec[vec.dim() - 1] << "\n";
    }
  };

  /**
   * @brief Small hook class that writes to file
   */
  template<typename Vector>
    class FileHook : public Hook<Vector>
  {
    using Real = typename Vector::Real;

    mutable std::shared_ptr<std::ofstream> file;
    std::size_t write_modulus;
    bool write_all;

    mutable std::size_t count = 0;

    public:

    /**
     * @brief Constructor
     *
     * @param filename       name of file for sample output
     * @param write_modulus_ write out every nth drawn sample
     * @param write_all_     whether to write all chains, or just the active one
     */
    FileHook(std::string filename = "ensample.out", std::size_t write_modulus_ = 1,
        bool write_all_ = false)
      : file(new std::ofstream(filename)), write_modulus(write_modulus_),
      write_all(write_all_)
    {}

    /**
     * @brief Enable or disable writing all chains to output file
     *
     * By default, only the currently active chain is written to file, so that
     * the file contains a list of accepted proposals and previous states where
     * the proposal was rejected. Setting this to true writes all other chains
     * as well, so that the file represents the current state of the whole
     * multi-chain MCMC algorithm.
     *
     * @param write_all_ whether all chains should be written, or just the current one
     */
    void set_write_all(bool write_all_)
    {
      write_all = write_all_;
    }

    std::string name() const override
    {
      return "file";
    }

    void call(const std::vector<Vector>& x, const std::vector<Real>& log_pi,
        std::size_t i_chain) const override
    {
      if (count++ % write_modulus == 0)
      {
        for (std::size_t i = 0; i < x[0].dim(); i++)
          *file << x[0][i] << " ";
        *file << log_pi[0];

        if (write_all)
          for (std::size_t j = 1; j < x.size(); j++)
          {
            *file << " ";

            for (std::size_t i = 0; i < x[j].dim(); i++)
              *file << x[j][i] << " ";
            *file << log_pi[j];
          }

        *file << "\n";
      }
    }
  };

  /**
   * @brief Small hook class that reports sample mean and variance
   */
  template<typename Vector>
    class MeanHook : public Hook<Vector>
  {
    using Real = typename Vector::Real;

    bool verbose;
    mutable Vector mean;
    mutable Vector square_sum;
    mutable Real count = 0.;

    public:

    /**
     * @brief Constructor
     *
     * @param verbose_ print mean and standard dev after each sample
     */
    MeanHook(bool verbose_ = true)
      : verbose(verbose_)
    {}

    std::string name() const override
    {
      return "mean";
    }

    void call(const std::vector<Vector>& x, const std::vector<Real>& log_pi,
        std::size_t i_chain) const override
    {
      const Vector& vec = x[i_chain];
      mean.setDim(vec.dim());
      square_sum.setDim(vec.dim());

      count += 1.;
      for (std::size_t i = 0; i < vec.dim(); i++)
      {
        const Real delta = vec[i] - mean[i];
        mean[i] += delta / count;
        const Real delta2 = vec[i] - mean[i];
        square_sum[i] += delta * delta2;
      }

      if (verbose)
      {
        std::cout << "x: ";
        for (std::size_t i = 0; i < vec.dim(); i++)
          std::cout << " " << vec[i];
        std::cout << " mean: ";
        for (std::size_t i = 0; i < vec.dim(); i++)
          std::cout << " " << mean[i];
        std::cout << " std dev: ";
        for (std::size_t i = 0; i < vec.dim(); i++)
          std::cout << " " << std::sqrt(square_sum[i] / (count-1));
        std::cout << "\n";
      }
    }

    /**
     * @brief Access to mean
     *
     * @return sample mean of MCMC chain
     */
    const Vector& get_mean() const
    {
      return mean;
    }

    /**
     * @brief Access to sum of squares
     *
     * This is the sum of squares of deviations.
     * Divide by count to obtain the sample variance.
     *
     * @return vector containing sum of squares
     */
    const Vector& get_square_sum() const
    {
      return square_sum;
    }

    /**
     * @brief Access to number of samples
     *
     * @return number of samples for sample mean
     */
    std::size_t get_count() const
    {
      return count;
    }
  };

#ifdef USE_GNUPLOT
  /**
   * @brief Hook class that opens real-time gnuplot window
   */
  template<typename Vector>
    class GnuplotHook : public Hook<Vector>
  {
    using Real = typename Vector::Real;

    std::shared_ptr<Gnuplot> gp;
    bool send_all, keep_data;

    mutable std::vector<std::vector<std::pair<Real,Real>>> data;

    mutable Real min_x =  std::numeric_limits<Real>::max();
    mutable Real max_x = -std::numeric_limits<Real>::max();
    mutable Real min_y =  std::numeric_limits<Real>::max();
    mutable Real max_y = -std::numeric_limits<Real>::max();

    public:

    /**
     * @brief Constructor
     *
     * @param send_all_  whether all chains should be visualized
     * @param keep_data_ whether full state history should be visualized
     */
    GnuplotHook(bool send_all_ = true, bool keep_data_ = false)
      : gp(new Gnuplot), send_all(send_all_), keep_data(keep_data_)
    {}

    /**
     * @brief Whether all chains should be visualized (default: true)
     *
     * @param send_all_ new value of Boolean flag
     */
    void set_send_all(bool send_all_)
    {
      send_all = send_all_;
    }

    /**
     * @brief Whether whole history of samples should be visualized (default: false)
     *
     * @param keep_data_ new value of Boolean flag
     */
    void set_keep_data(bool keep_data_)
    {
      keep_data = keep_data_;
    }

    std::string name() const override
    {
      return "gnuplot";
    }

    void call(const std::vector<Vector>& x, const std::vector<Real>& log_pi,
        std::size_t i_chain) const override
    {
      if (!keep_data)
        for (auto& entry : data)
          entry.clear();

      if (send_all)
      {
        data.resize(x.size());
        for (std::size_t i = 0; i < x.size(); i++)
          x[i].plot(data[i],min_x,max_x,min_y,max_y);
      }
      else
      {
        data.resize(1);
        x[i_chain].plot(data[0],min_x,max_x,min_y,max_y);
      }

      *gp << "set xrange[" << min_x << ":" << max_x << "]\n"
        << "set yrange[" << min_y << ":" << max_y << "]\n";

      *gp << "plot '-' w l t 'x'";
      if (send_all)
        for (std::size_t i = 1; i < data.size(); i++)
          *gp << ", '-' w l t 'x [" << i+1 << "]'";
      *gp << "\n";

      if (send_all)
        for (std::size_t i = 0; i < data.size(); i++)
          gp->send1d(data[i]);
      else
        gp->send1d(data[0]);

      gp->flush();
    }
  };
#endif // USE_GNUPLOT

} // namespace enSample

#endif // ENSAMPLE_HOOKS_HH
