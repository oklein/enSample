#ifndef ENSAMPLE_ENSAMPLE_HH
#define ENSAMPLE_ENSAMPLE_HH

#include<iostream>
#include<vector>
#include<chrono>

#include "random.hh"
#include "logdensities.hh"
#include "kernels.hh"
#include "hooks.hh"

namespace enSample {

  /**
   * @brief Ensemble MCMC algorithm
   */
  template<typename LogDensity>
    class Sampler
    {
      private:

        // types defining vector space
        using Vector = typename LogDensity::Vector;
        using Real   = typename LogDensity::Real;

        // log density object
        LogDensity log_density;

        // kernels, their weights, and their activation counts
        std::vector<std::tuple<TransitionKernel<Vector>*, Real,
          std::size_t, std::size_t>> kernels;

        // current positions and proposals
        std::vector<Vector> x;
        Vector x_prop;
        std::size_t n_chain;
        std::size_t i_chain;
        bool cyclic = false;

        // hook function object
        bool hook_rejected;
        std::vector<Hook<Vector>*> hooks;

        // log densities, log Hastings ratio, and acceptance threshold
        std::vector<Real> log_pi;
        Real log_pi_prop, log_hastings, threshold;

        // log density gradients (if needed)
        bool needs_gradient = false;
        std::vector<Vector> grad_log_pi;

        // mask for active components
        //std::vector<bool> active;
        std::vector<std::size_t> active;
        Real target_active = std::numeric_limits<Real>::max();
        bool active_ratio_fixed = false;

        // bookkeeping
        std::size_t moved_comp = 0, num_steps = 0, accepted = 0;
        bool initialized = false, verbose;

      public:

        /**
         * @brief Constructor
         *
         * @param log_density_   logarithm of density we want to sample
         * @param n_chain_       number of chains to run simultaneously
         * @param verbose_       enable printing of status information, timings
         * @param hook_rejected_ whether hooks shoud run when old state is kept
         */
        Sampler(const LogDensity& log_density_, std::size_t n_chain_ = 2,
            bool verbose_ = true, bool hook_rejected_ = true)
          : log_density(log_density_), n_chain(n_chain_),
          hook_rejected(hook_rejected_), verbose(verbose_)
      {}

        /**
         * @brief Destructor
         */
        ~Sampler()
        {
          for (auto&& kernel : kernels)
            delete std::get<0>(kernel);
          for (auto&& hook : hooks)
            delete hook;
        }

        /**
         * @brief Remove all kernels
         */
        void clear_kernels()
        {
          for (auto&& kernel : kernels)
            delete std::get<0>(kernel);

          kernels.clear();

          needs_gradient = false;
          grad_log_pi.clear();
        }

        /**
         * @brief Remove all hooks
         */
        void clear_hooks()
        {
          for (auto&& hook : hooks)
            delete hook;

          hooks.clear();
        }

        /**
         * @brief Create a t-walk sampler
         *
         * This configures the sampler as a scale-invariant t-walk MCMC algorithm.
         * The default arguments create a t-walk as it was presented in the
         * original publication.
         *
         * @param a_w walk move step parameter
         * @param a_t traverse move step parameter
         * @param n_1 number of active components
         */
        void set_twalk(Real a_w = 1.5, Real a_t = 6., Real n_1 = 4.)
        {
          clear_kernels();

          kernels.emplace_back(new TWalk::WalkKernel    <Vector>(a_w),60./122.,0,0);
          kernels.emplace_back(new TWalk::TraverseKernel<Vector>(a_t),60./122.,0,0);
          kernels.emplace_back(new TWalk::HopKernel     <Vector>(),   1./122. ,0,0);
          kernels.emplace_back(new TWalk::BlowKernel    <Vector>(),   1./122. ,0,0);

          set_target_active_components(n_1);
          set_cyclic_chains(false);
          set_fixed_active_ratio(false);
        }

        /**
         * @brief Create a Goodman & Weare sampler
         *
         * This configures the sampler as an affine-invariant Ensemble Sampler.
         * Currently only the walk move is provided here, but the t-walk
         * walk move the t-walk is very similar to the G&W stretch move, and may
         * potentially serve as a replacement.
         */
        void set_goodman_weare()
        {
          clear_kernels();

          kernels.emplace_back(new GoodmanWeare::WalkKernel<Vector>(),1.,0,0);

          set_target_active_components(std::numeric_limits<Real>::max());
          set_fixed_active_ratio(true);
          set_cyclic_chains(true);
        }

        /**
         * @brief Create a Differential Evolution sampler
         *
         * This configures the sampler as a Differential Evolution sampler.
         *
         * @param b       variance of noise in DE move proposal
         * @param gamma   DE move step parameter
         * @param gamma_s DE snooker move step parameter
         */
        void set_differential_evolution(Real b = 1e-4, Real gamma = 2.38, Real gamma_s = 1.7)
        {
          clear_kernels();

          kernels.emplace_back(new DiffEvolution::DEKernel<Vector>(b,gamma),       0.9,0,0);
          kernels.emplace_back(new DiffEvolution::DESnookerKernel<Vector>(gamma_s),0.1,0,0);

          set_target_active_components(std::numeric_limits<Real>::max());
          set_fixed_active_ratio(true);
          set_cyclic_chains(false);
        }

        /**
         * @brief Add new kernel to setup (copy existing kernel)
         *
         * @param weight kernels are chosen proportional to their weight
         * @param kernel instance of kernel object that should be copied
         */
        template<template<typename Vector> class Kernel>
          void add_kernel(Real weight, Kernel<Vector> kernel)
          {
            kernels.emplace_back(new Kernel<Vector>(kernel),weight,0,0);

            if (std::get<0>(kernels.back())->needs_gradient())
              needs_gradient = true;
          }

        /**
         * @brief Add new kernel to setup (pass constructor arguments)
         *
         * @param weight kernels are chosen proportional to their weight
         * @param args   arguments that are passed to kernel constructor
         */
        template<template<typename Vector> class Kernel, typename... Args>
          void add_kernel(Real weight, Args... args)
          {
            kernels.emplace_back(new Kernel<Vector>(args...),weight,0,0);

            if (std::get<0>(kernels.back())->needs_gradient())
              needs_gradient = true;
          }

        /**
         * @brief Report number of configured kernels
         *
         * @return number of kernels
         */
        std::size_t num_kernels() const
        {
          return kernels.size();
        }

        /**
         * @brief Report information for one of the kernels
         *
         * @return tuple of kernel weight, name, counted steps and accepted steps
         */
        std::tuple<std::string,Real,std::size_t,std::size_t>
          kernel_info(std::size_t i) const
          {
            if (i >= kernels.size())
              throw std::domain_error("kernel index to large");

            return {std::get<0>(kernels[i])->name(), std::get<1>(kernels[i]),
              std::get<2>(kernels[i]), std::get<3>(kernels[i])};
          }

        /**
         * @brief Print kernel setup
         */
        void print_kernels() const
        {
          for (const auto& kernel : kernels)
            std::cout << "name: " << std::get<0>(kernel)->name()
              << " weight: " << std::get<1>(kernel) << std::endl;
        }

        /**
         * @brief Add new hook to setup (copy existing hook)
         *
         * @param hook instance of hook object that should be copied
         */
        template<template<typename Vector> class Hook>
          void add_hook(Hook<Vector> hook)
          {
            hooks.emplace_back(new Hook<Vector>(hook));
          }

        /**
         * @brief Add new hook to setup (pass constructor arguments)
         *
         * @param args arguments that are passed to hook constructor
         */
        template<template<typename Vector> class Hook, typename... Args>
          void add_hook(Args... args)
          {
            hooks.emplace_back(new Hook<Vector>(args...));
          }

        /**
         * @brief Report number of configured hooks
         *
         * @return number of hooks
         */
        std::size_t num_hooks() const
        {
          return hooks.size();
        }

        /**
         * @brief Access one of the hook objects
         *
         * This method returns one of the hooks as base class reference.
         * This reference can be converted to a reference of a specific
         * hook class using dynamic_cast, if access to special
         * functionality and methods is needed.
         *
         * @return constant reference of type Hook<Vector>
         */
        const Hook<Vector>& hook(std::size_t i) const
        {
          if (i >= hooks.size())
            throw std::domain_error("hook index too large");

          return *(hooks[i]);
        }

        /**
         * @brief Set whether active chain is picked cyclically or at random
         *
         * Some multi-chain algorithms cycle through the chains in a
         * fixed order, while others, e.g., the t-walk, randomly pick a
         * chain to modify in each step. Enable or disable cyclic chains
         * (default: non-cyclic, i.e., random selection).
         *
         * @param cyclic_ whether cyclic chains should be used
         */
        void set_cyclic_chains(bool cyclic_)
        {
          cyclic = cyclic_;
        }

        /**
         * @brief Report whether chains are cyclic
         *
         * @return status of Boolean flag
         *
         * @see set_cyclic_chains
         */
        bool cyclic_chains() const
        {
          return cyclic;
        }

        /**
         * @brief Set expected number of active components per step
         *
         * The number of active components per step, i.e., vector
         * components that are modified during proposal (default: all
         * of them). Effectively a projection of the method onto a
         * subspace, but a new set of components is drawn for each step.
         * This is a real number, but only the integer part is used
         * if set_fixed_active_ratio was enabled.
         *
         * @param n_1 number of components that should be active
         */
        void set_target_active_components(Real n_1)
        {
          target_active = n_1;
        }

        /**
         * @brief Report current expected number of active components
         *
         * @return target number of active components
         *
         * @see set_target_active_components
         */
        Real target_active_components() const
        {
          return target_active;
        }

        /**
         * @brief Set whether ratio of active components is fixed
         *        or mildly randomized
         *
         * Whether or not a given component is active during a step can
         * be determined independently of the others by drawing from an
         * appropriate distribution, or jointly by drawing from a reservoir.
         * The former leads to a number of active components that may
         * fluctuate around the target value, while the latter always
         * activates exactly the same number of components. Enable or
         * disable such a fixed ratio (default: non-fixed ratio, i.e.,
         * independent activation).
         *
         * @param active_ratio_fixed_ whether reservoir sampling should be used
         */
        void set_fixed_active_ratio(bool active_ratio_fixed_)
        {
          active_ratio_fixed = active_ratio_fixed_;
        }

        /**
         * @brief Report whether ratio of active components is fixed
         *        or not mildly randomized
         *
         * @return status of Boolean flag
         *
         * @see set_fixed_active_ratio
         */
        bool fixed_active_ratio() const
        {
          return active_ratio_fixed;
        }

        /**
         * @brief Simulate Markov chain with default initial positions
         *
         * Set up the MCMC sampler, obtain random or fixed starting
         * positions for the chains from the log-density object, make
         * sure that these positions actually are situated within the
         * support of the log-density function, then simulate a fixed
         * number of steps of the multi-chain MCMC algorithm.
         *
         * @param steps number of steps to perform
         */
        void simulate(int steps)
        {
          x.resize(n_chain);
          log_density.initialize(x);
          simulate(steps,x);
        }

        /**
         * @brief Simulate Markov chain with custom initial positions
         *
         * Same as simulate(int steps), but with custom starting
         * positions.
         */
        void simulate(std::size_t steps, const std::vector<Vector>& x_)
        {	
          auto start_sys = std::chrono::system_clock::now();
          std::time_t start_time = std::chrono::system_clock::to_time_t(start_sys);
          auto start = std::chrono::steady_clock::now();

          if (verbose)
            std::cout << "started computation at " << std::ctime(&start_time) << "\n";

          initialize(x_);

          for (std::size_t i = 0; i < steps; i++)
            step();

          auto end_sys = std::chrono::system_clock::now();
          std::time_t end_time = std::chrono::system_clock::to_time_t(end_sys);
          auto end = std::chrono::steady_clock::now();
          std::chrono::duration<double> elapsed_seconds = end - start;

          if (verbose)
          {
            std::cout << "finished computation at " << std::ctime(&end_time)
              << "elapsed time: " << elapsed_seconds.count() << " s ("
              << elapsed_seconds.count()/steps << " s/step)\n";

            print_statistics();
          }
        }

        /**
         * @brief Perform a single Markov chain step
         *
         * Perform a single step of the multi-chain MCMC algorithm. May
         * only be called once the chains have been initialized, which
         * can be achieved by calling simulate(0). This function is
         * intended for situations where tight control over the generated
         * samples is needed and the simulate methods aren't flexible enough.
         *
         * @return the new sample (proposal if accepted, else previous state)
         */
        const Vector& step()
        {
          if (!initialized)
            throw std::logic_error("initialize sampler by calling simulate(0) first");

          // select active MCMC chain
          select_active_chain();

          // select transition kernel and active components
          std::size_t i_kernel = select_kernel();
          TransitionKernel<Vector>* kernel = std::get<0>(kernels[i_kernel]);
          select_active_components();

          // create proposal and compute acceptance probability
          kernel->propose(x,i_chain,active,grad_log_pi,x_prop);

          log_pi_prop = log_density.value(x_prop);
          if (needs_gradient)
            log_density.gradient(x_prop,grad_log_pi.back());

          log_hastings = kernel->log_hastings(x,i_chain,active,x_prop,grad_log_pi);

          if(std::isfinite(log_pi_prop))
            threshold = std::exp(log_pi_prop - log_pi[i_chain] + log_hastings);
          else
            threshold = 0.;

          // step accepted
          if (rng<Real>.uniform_0_1() < threshold)
          {
            moved_comp += active.size();
            accepted++;
            std::get<3>(kernels[i_kernel])++;

            x[i_chain] = x_prop;
            log_pi[i_chain] = log_pi_prop;

            if (needs_gradient)
              grad_log_pi[i_chain] = grad_log_pi.back();

            for (const auto& hook : hooks)
              hook->call(x,log_pi,i_chain);
          }
          // step rejected
          else
          {
            if (hook_rejected)
              for (const auto& hook : hooks)
                hook->call(x,log_pi,i_chain);
          }
          num_steps++;

          return x[i_chain];
        }

        /**
         * @brief Provide acceptance probabilities, etc.
         *
         * Provide statistics about the MCMC run (number of steps,
         * number of accepted steps, number of moved components,
         * utilization of kernels, etc.). The return value contains
         * one vector of numbers (number of steps, accepted steps,
         * total moved components) for the whole sampler, and one
         * vector each (number of steps, accepted steps) per kernel.
         *
         * @return vector of vector of real numbers, see above
         */
        std::vector<std::vector<Real>> provide_statistics() const
        {
          std::vector<std::vector<Real>> out(kernels.size() + 1);

          out[0].push_back(num_steps);
          out[0].push_back(accepted);
          out[0].push_back(moved_comp);

          for (std::size_t i = 0; i < kernels.size(); i++)
          {
            out[i+1].push_back(std::get<2>(kernels[i]));
            out[i+1].push_back(std::get<3>(kernels[i]));
          }

          return out;
        }

        /**
         * @brief Report on acceptance probabilities, etc.
         *
         * Print statistics about the MCMC run (percentage of
         * accepted steps, number of moved components, utilization
         * of kernels, etc.). Is automatically called by simulate(steps)
         * and simulate(steps,x_), and may be called by hand when using
         * step() to advance the chains.
         */
        void print_statistics() const
        {
          std::cout << "accepted " << accepted
            << " steps (" << accepted/Real(num_steps) * 100.
            << " %), moved " << moved_comp
            << " components\n    (avg. " << moved_comp/Real(num_steps)
            << " comp/step, " << moved_comp/Real(accepted)
            << " comp/acc. step, " << moved_comp/Real(accepted*x[0].dim()) * 100.
            << " %)\n";

          std::cout << "kernels:\n";
          for (std::size_t i = 0; i < kernels.size(); i++)
            std::cout << "name: " << std::get<0>(kernels[i])->name()
              << " weight: " << std::get<1>(kernels[i])
              << " count: " << std::get<2>(kernels[i])
              << " (" << std::get<2>(kernels[i])/Real(num_steps) * 100.
              << " %, " << std::get<3>(kernels[i])/Real(std::get<2>(kernels[i])) * 100.
              << " % accepted)\n";
        }

      private:

        /**
         * @brief Make sure that initial positions are valid
         *
         * @param x_ vector of starting positions
         */
        void initialize(const std::vector<Vector>& x_)
        {
          // compute initial log densities
          x = x_;
          log_pi.resize(n_chain);

          for (std::size_t i = 0; i < n_chain; i++)
          {
            log_pi[i] = log_density.value(x[i]);

            if(!std::isfinite(log_pi[0]))
              throw std::domain_error("initial x not in support");
          }

          // compute initial gradients (if needed)
          if (needs_gradient)
          {
            // last entry is for grad_log_pi_prop!
            grad_log_pi.resize(n_chain + 1);

            for (std::size_t i = 0; i < n_chain; i++)
            {
              grad_log_pi[i] = x[0];
              log_density.gradient(x[i],grad_log_pi[i]);
            }

            grad_log_pi.back().setDim(x[0].dim());
          }

          // initialize proposal with current vector
          x_prop = x[0];
          log_pi_prop = log_pi[0];

          // reset bookkeeping
          if (kernels.empty())
            throw std::logic_error("no transition kernels configured");
          for (auto&& kernel : kernels)
          {
            std::get<2>(kernel) = 0;
            std::get<3>(kernel) = 0;
          }

          moved_comp = 0;
          num_steps = 0;
          accepted = 0;

          initialized = true;
        }

        /**
         * @brief Select a kernel according to their weights
         *
         * @return index of kernel that was selected
         */
        std::size_t select_kernel()
        {
          Real total_weight = 0.;
          for (const auto& kernel : kernels)
            total_weight += std::get<1>(kernel);
          const Real threshold = rng<Real>.uniform_0_1() * total_weight;

          Real summed_weight = 0.;
          for (std::size_t i = 0; i < kernels.size(); i++)
          {
            summed_weight += std::get<1>(kernels[i]);
            if (summed_weight > threshold)
            {
              std::get<2>(kernels[i])++;
              return i;
            }
          }

          // catch-all for weird rounding errors
          const std::size_t last = kernels.size() - 1;
          std::get<2>(kernels[last])++;
          return last;
        }

        /**
         * @brief Select number of active components
         */
        void select_active_components()
        {

          // pick exactly target_active active components
          if (active_ratio_fixed)
            reservoir_sample(x[i_chain].dim(),target_active,active);
          // activate individual components independently
          else
          {
            active.clear();
            const Real p_active = std::min(1., target_active/x[i_chain].dim());

            for (std::size_t i = 0; i < x[i_chain].dim(); i++)
              if (rng<Real>.uniform_0_1() < p_active)
                active.push_back(i);
          }
        }

        /**
         * @brief Select active MCMC chain
         */
        void select_active_chain()
        {
          // cycle throug chains
          if (cyclic)
            i_chain = (i_chain + 1) % n_chain;
          // pick chain at random
          else
          {
            i_chain = rng<Real>.uniform_0_1() * Real(n_chain);

            // exclude corner case
            i_chain = std::min(n_chain-1,i_chain);
          }
        }

        /**
         * @brief Draw set of distinct indices with equal probability
         */
        void reservoir_sample(std::size_t n_max, std::size_t n_indices,
            std::vector<std::size_t>& indices)
        {
          indices.resize(n_indices);
          for (std::size_t i = 0; i < n_indices; i++)
            indices[i] = i;

          for (std::size_t i = n_indices; i < n_max; i++)
          {
            std::size_t j = rng<Real>.uniform_0_1() * (i + 1);
            if (j < n_indices)
              indices[j] = i;
          }
        }

    };

} // namespace enSample

#endif // ENSAMPLE_ENSAMPLE_HH
