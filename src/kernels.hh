#ifndef ENSAMPLE_KERNELS_HH
#define ENSAMPLE_KERNELS_HH

#include<algorithm>

#ifdef HAVE_TINYSPLINE
#include "tinysplinecpp.h"
#endif // HAVE_TINYSPLINE

#include "random.hh"

namespace enSample {

  /**
   * @brief Abstract base class for transition kernels
   */
  template<class Vector>
    class TransitionKernel
    {
      protected:

        /// @brief scalar data type for coordinates and parameters
        using Real = typename Vector::Real;

      public:

        virtual ~TransitionKernel() {};

        /**
         * @brief Name of kernel
         */
        virtual std::string name() const = 0;

        /**
         * @brief Set to true if gradients of log pi should be computed
         */
        virtual bool needs_gradient() const {return false;}

        /**
         * @brief Proposal function h(x,x')
         *
         * @param      x           vector of current MCMC chain states
         * @param      i_chain     index of state that should move
         * @param      active      vector of currently active components
         * @param      grad_log_pi gradients of log-density (e.g., for MALA)
         * @param[out] x_prop      proposal for state update
         */
        virtual void propose(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active,
            const std::vector<Vector>& grad_log_pi, Vector& x_prop) const = 0;

        /**
         * @brief log of hastings ratio g(y|x,z) / g(x|y,z)
         *
         * @param      x           vector of current MCMC chain states
         * @param      i_chain     index of state that should move
         * @param      active      vector of currently active components
         * @param      x_prop      proposal for state update
         * @param      grad_log_pi gradients of log-density (e.g., for MALA)
         * @return                 logarithm of Hastings ratio
         */
        virtual Real log_hastings(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active, const Vector& x_prop,
            const std::vector<Vector>& grad_log_pi) const = 0;

      protected:

        /**
         * @brief Vector difference operator
         *
         * @param      x   a state vector
         * @param      y   another state vector
         * @param[out] out difference x - y
         */
        static void difference(const Vector& x, const Vector& y, Vector& out)
        {
          if (x.dim() != y.dim())
            throw std::domain_error("x and y have different dimensions");

          out.setDim(x.dim());

          for (std::size_t i = 0; i < out.dim(); i++)
            out[i] = x[i] - y[i];
        }

        /**
         * @brief Maximum of active component absolute values
         *
         * @param      x      a state vector
         * @param      active vector of currently active components
         * @return            maximum absolute value
         */
        static std::size_t active_max(const Vector& x, const std::vector<std::size_t>& active)
        {
          std::size_t index = 0;

          for (const auto& comp : active)
            if (std::abs(x[index]) < std::abs(x[comp]))
              index = comp;

          return index;
        }

        /**
         * @brief Draw n_indices indices uniformly, excluding i_chain
         *
         * @param      n_max     upper bound of index set
         * @param      n_indices number of indices to draw
         * @param      i_chain   index to omit while drawing
         * @param[out] indices   n_indices indices from [0,n_max]
         */
        static void reservoir_sample(std::size_t n_max, std::size_t n_indices,
            std::size_t i_chain, std::vector<std::size_t>& indices)
        {
          indices.resize(n_indices);
          for (std::size_t i = 0; i < n_indices; i++)
            indices[i] = i;

          for (std::size_t i = n_indices; i < n_max - 1; i++)
          {
            std::size_t j = rng<Real>.uniform_0_1() * (i + 1);
            if (j < n_indices)
              indices[j] = i;
          }

          for (std::size_t i = 0; i < n_indices; i++)
            if (indices[i] >= i_chain)
              indices[i]++;
        }
    };

  /**
   * @brief Classical Metropolis Random Walk (MRW) isotropic proposal kernel
   */
  template<class Vector>
    class MetropolisKernel: public TransitionKernel<Vector>
  {
    private:

      using typename TransitionKernel<Vector>::Real;

      Real sigma;

    public:

      /**
       * @brief Constructor
       *
       * @param sigma_ standard deviation of the proposal noise
       */
      MetropolisKernel(Real sigma_ = 1.)
        : sigma(sigma_)
      {}

      std::string name() const override
      {
        return "metropolis";
      }

      void propose(const std::vector<Vector>& x, std::size_t i_chain,
          const std::vector<std::size_t>& active,
          const std::vector<Vector>& grad_log_pi, Vector& x_prop) const override
      {
        const Vector& x_0 = x[i_chain];
        x_prop.setDim(x_0.dim());

        x_prop = x_0;
        for (const auto& comp : active)
          x_prop[comp] += rng<Real>.normal(0.,sigma);
      }

      Real log_hastings(const std::vector<Vector>& x, std::size_t i_chain,
          const std::vector<std::size_t>& active, const Vector& x_prop,
          const std::vector<Vector>& grad_log_pi) const override
      {
        return 0.;
      }
  };

  /**
   * @brief Metropolis-adjusted Langevin (MALA) proposal kernel
   */
  template<typename Vector>
    class MALAKernel : public TransitionKernel<Vector>
  {
    private:

      using typename TransitionKernel<Vector>::Real;

      mutable Vector diff;
      Real tau;
      Real sqrt_two_tau;

    public:

      /**
       * @brief Constructor
       *
       * @param tau_ MALA time step parameter
       */
      MALAKernel(Real tau_ = 1.)
        : tau(tau_), sqrt_two_tau(std::sqrt(2.*tau))
      {}

      std::string name() const override
      {
        return "mala";
      }

      bool needs_gradient() const override
      {
        return true;
      }

      void propose(const std::vector<Vector>& x, std::size_t i_chain,
          const std::vector<std::size_t>& active,
          const std::vector<Vector>& grad_log_pi, Vector& x_prop) const override
      {
        const Vector& x_0 = x[i_chain];
        const Vector& grad = grad_log_pi[i_chain];
        x_prop.setDim(x_0.dim());

        x_prop = x_0;
        for (const auto& comp : active)
          x_prop[comp] += tau * grad[comp] + sqrt_two_tau * rng<Real>.normal(0.,1.);
      }

      Real log_hastings(const std::vector<Vector>& x, std::size_t i_chain,
          const std::vector<std::size_t>& active, const Vector& x_prop,
          const std::vector<Vector>& grad_log_pi) const override
      {
        return log_g(x_prop,x[i_chain],grad_log_pi.back(),active)
          - log_g(x[i_chain],x_prop,grad_log_pi[i_chain],active);
      }

    private:

      Real log_g(const Vector& x, const Vector& x_prime, const Vector& grad,
          const std::vector<std::size_t>& active) const
      {
        if (x.dim() != x_prime.dim())
          throw std::domain_error("x and x_prime must have consistend dimensions");

        this->difference(x_prime,x,diff);

        Real sum = 0.;

        for (const auto& comp : active)
          sum += (diff[comp] - tau * grad[comp]) * (diff[comp] - tau * grad[comp]);

        return - 0.25/tau * sum;
      }
  };

  /**
   * @brief T-Walk proposal kernels (Christen & Fox, 2010)
   */
  namespace TWalk {

    /**
     * @brief T-Walk Walk proposal kernel
     */
    template<class Vector>
      class WalkKernel: public TransitionKernel<Vector>
    {
      private:

        using typename TransitionKernel<Vector>::Real;

        Real a_w;

      public:

        /**
         * @brief Constructor
         *
         * @param a_w_ walk move step parameter >= 0
         */
        WalkKernel(Real a_w_)
          : a_w(a_w_)
        {
          if (a_w < 0.)
            throw std::domain_error("a_w needs to be non-negative");
        }

        std::string name() const override
        {
          return "t-walk walk";
        }

        void propose(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active,
            const std::vector<Vector>& grad_log_pi, Vector& x_prop) const override
        {
          if (x.size() < 2)
            throw std::domain_error("walk kernel needs at least two states");

          std::size_t i_partner = rng<Real>.uniform_0_1() * (x.size() - 1);
          if (i_partner >= i_chain)
            i_partner++;

          const Vector& x_0 = x[i_chain];
          const Vector& x_1 = x[i_partner];

          if (x_0.dim() != x_1.dim())
            throw std::domain_error("x and x_prime have different dimensions");

          x_prop.setDim(x_0.dim());

          x_prop = x_0;
          for (const auto& comp : active)
            x_prop[comp] += psi_w() * (x_0[comp] - x_1[comp]);
        }

        Real log_hastings(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active, const Vector& x_prop,
            const std::vector<Vector>& grad_log_pi) const override
        {
          return 0.;
        }

      private:

        Real psi_w() const
        {
          Real u = rng<Real>.uniform_0_1();
          return a_w * (-1. + 2.*u + a_w*u*u) / (1. + a_w);
        }
    };

    /**
     * @brief T-Walk Traverse proposal kernel
     */
    template<class Vector>
      class TraverseKernel: public TransitionKernel<Vector>
    {
      private:

        using typename TransitionKernel<Vector>::Real;

        Real a_t;
        mutable Real beta;

      public:

        /**
         * @brief Constructor
         *
         * @param a_t_ traverse move step parameter >= 1
         */
        TraverseKernel(Real a_t_)
          : a_t(a_t_)
        {
          if (a_t < 1.)
            throw std::domain_error("a_t needs to be >= 1");
        }

        std::string name() const override
        {
          return "t-walk traverse";
        }

        void propose(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active,
            const std::vector<Vector>& grad_log_pi, Vector& x_prop) const override
        {
          if (x.size() < 2)
            throw std::domain_error("traverse kernel needs at least two states");

          std::size_t i_partner = rng<Real>.uniform_0_1() * (x.size() - 1);
          if (i_partner >= i_chain)
            i_partner++;

          const Vector& x_0 = x[i_chain];
          const Vector& x_1 = x[i_partner];

          if (x_0.dim() != x_1.dim())
            throw std::domain_error("x and x_prime have different dimensions");

          x_prop.setDim(x_0.dim());

          beta = psi_t();

          x_prop = x_0;
          for (const auto& comp : active)
            x_prop[comp] += (1. + beta) * (x_1[comp] - x_0[comp]);
        }

        Real log_hastings(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active, const Vector& x_prop,
            const std::vector<Vector>& grad_log_pi) const override
        {
          return (active.size() - 2.) * std::log(beta);
        }

      private:

        Real psi_t() const
        {
          const Real u = rng<Real>.uniform_0_1();

          if (rng<Real>.uniform_0_1() < 0.5 * (1. - 1./a_t))
            return std::pow(u,1./(a_t+1.));
          else
            return std::pow(u,1./(1.-a_t));
        }
    };


    /**
     * @brief T-Walk Hop proposal kernel
     */
    template<class Vector>
      class HopKernel: public TransitionKernel<Vector>
    {
      private:

        using typename TransitionKernel<Vector>::Real;

        mutable Real sigma;
        mutable Vector diff;
        mutable std::size_t i_partner;

      public:

        std::string name() const override
        {
          return "t-walk hop";
        }

        void propose(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active,
            const std::vector<Vector>& grad_log_pi, Vector& x_prop) const override
        {
          if (x.size() < 2)
            throw std::domain_error("hop kernel needs at least two states");

          std::size_t i_partner = rng<Real>.uniform_0_1() * (x.size() - 1);
          if (i_partner >= i_chain)
            i_partner++;

          const Vector& x_0 = x[i_chain];
          const Vector& x_1 = x[i_partner];

          if (x_0.dim() != x_1.dim())
            throw std::domain_error("x and x_prime have different dimensions");

          x_prop.setDim(x_0.dim());

          this->difference(x_1,x_0,diff);
          std::size_t index = this->active_max(diff,active);

          sigma = std::abs(diff[index])/3.;

          x_prop = x_0;
          for (const auto& comp : active)
            x_prop[comp] += sigma * rng<Real>.normal_0_1();
        }

        Real log_hastings(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active, const Vector& x_prop,
            const std::vector<Vector>& grad_log_pi) const override
        {
          return log_g(x_prop,x[i_chain],x[i_partner],active)
            - log_g(x[i_chain],x_prop,x[i_partner],active);
        }

      private:

        Real log_g(const Vector& y, const Vector& x, const Vector& x_prime,
            const std::vector<std::size_t>& active) const
        {
          if (y == x)
            return - std::numeric_limits<Real>::infinity();

          if (x.dim() != x_prime.dim() || x_prime.dim() != y.dim())
            throw std::domain_error("x, x_prime and y must have consistent dimensions");

          this->difference(x_prime,x,diff);
          std::size_t index = this->active_max(diff,active);

          sigma = std::abs(diff[index]);

          Real sum = 0.;

          for (const auto& comp : active)
            sum += (y[comp]-x[comp]) * (y[comp]-x[comp]);

          return - active.size() * (0.5*std::log(2.*M_PI) + std::log(sigma/3.)) - 4.5/(sigma*sigma) * sum;
        }
    };


    /**
     * @brief T-Walk Blow proposal kernel
     */
    template<class Vector>
      class BlowKernel: public TransitionKernel<Vector>
    {
      private:

        using typename TransitionKernel<Vector>::Real;

        mutable Real sigma;
        mutable Vector diff;
        mutable std::size_t i_partner;

      public:

        std::string name() const override
        {
          return "t-walk blow";
        }

        void propose(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active,
            const std::vector<Vector>& grad_log_pi, Vector& x_prop) const override
        {
          if (x.size() < 2)
            throw std::domain_error("blow kernel needs two states");

          std::size_t i_partner = rng<Real>.uniform_0_1() * (x.size() - 1);
          if (i_partner >= i_chain)
            i_partner++;

          const Vector& x_0 = x[i_chain];
          const Vector& x_1 = x[i_partner];

          if (x_0.dim() != x_1.dim())
            throw std::domain_error("x and x_prime have different dimensions");

          x_prop.setDim(x_0.dim());

          this->difference(x_1,x_0,diff);
          std::size_t index = this->active_max(diff,active);

          sigma = std::abs(diff[index]);

          x_prop = x_0;
          for (const auto& comp : active)
            x_prop[comp] += (x_1[comp] - x_0[comp]) + sigma * rng<Real>.normal_0_1();
        }

        Real log_hastings(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active, const Vector& x_prop,
            const std::vector<Vector>& grad_log_pi) const override
        {
          return log_g(x_prop,x[i_chain],x[i_partner],active)
            - log_g(x[i_chain],x_prop,x[i_partner],active);
        }

      private:

        Real log_g(const Vector& y, const Vector& x, const Vector& x_prime,
            const std::vector<std::size_t>& active) const
        {
          if (y == x)
            return - std::numeric_limits<Real>::infinity();

          if (x.dim() != x_prime.dim() || x_prime.dim() != y.dim())
            throw std::domain_error("x, x_prime and y must have consistent dimensions");

          this->difference(x_prime,x,diff);
          std::size_t index = this->active_max(diff,active);

          sigma = std::abs(diff[index]);

          Real sum = 0.;

          for (const auto& comp : active)
            sum += (y[comp]-x_prime[comp]) * (y[comp]-x_prime[comp]);

          return - active.size() * (0.5*std::log(2.*M_PI) + std::log(sigma)) - 0.5/(sigma*sigma) * sum;
        }
    };

  } // namespace TWalk

  /**
   * @brief Goodman & Weare ensemble proposal kernels (Goodman & Weare, 2010)
   */
  namespace GoodmanWeare {

    /**
     * @brief Goodman & Weare Walk proposal kernel
     */
    template<class Vector>
      class WalkKernel: public TransitionKernel<Vector>
    {
      private:

        using typename TransitionKernel<Vector>::Real;

        mutable std::vector<std::size_t> indices;
        mutable Vector x_mean;

      public:

        std::string name() const override
        {
          return "G&W walk";
        }

        void propose(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active,
            const std::vector<Vector>& grad_log_pi, Vector& x_prop) const override
        {
          if (x.size() < 3)
            throw std::domain_error("walk kernel needs at least three states");

          std::size_t n_indices = 2 + rng<Real>.uniform_0_1() * (x.size() - 2);
          this->reservoir_sample(x.size(),n_indices,i_chain,indices);

          get_mean(x);
          x_prop.setDim(x_mean.dim());

          for (const auto& i : indices)
          {
            Real z = rng<Real>.normal_0_1();

            x_prop = x[i_chain];
            for (const auto& comp : active)
              x_prop[comp] += z * (x[i][comp] - x_mean[comp]);
          }
        }

        Real log_hastings(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active, const Vector& x_prop,
            const std::vector<Vector>& grad_log_pi) const override
        {
          return 0.;
        }

      private:

        void get_mean(const std::vector<Vector>& x) const
        {
          x_mean.setDim(x[0].dim());
          for (std::size_t comp = 0; comp < x_mean.dim(); comp++)
          {
            x_mean[comp] = 0.;
            for (const auto& i : indices)
              x_mean[comp] += x[i][comp];
            x_mean[comp] /= indices.size();
          }
        }
    };

  } // namespace GoodmanWeare

  /**
   * @brief Differential evolution proposal kernels (Ter Braak, 2006 & 2008)
   */
  namespace DiffEvolution {

    /**
     * @brief Differential Evolution Markov Chain proposal kernel
     */
    template<typename Vector>
      class DEKernel : public TransitionKernel<Vector>
    {
      private:

        using typename TransitionKernel<Vector>::Real;

        Real sqrt_b, gamma_0;
        bool adapt_dim, large_jumps;

        mutable Real gamma;
        mutable std::vector<std::size_t> indices;

      public:

        /**
         * @brief Constructor
         *
         * @param b            variance of the proposal noise
         * @param gamma_0_     dimension-independent DE step parameter
         * @param adapt_dim_   set gamma = gamma_0/sqrt(2d) if true, else gamma_0
         * @param large_jumps_ sporadically perform large jumps if true (1/10 steps)
         */
        DEKernel(Real b = 1e-4, Real gamma_0_ = 2.38, bool adapt_dim_ = true,
            bool large_jumps_ = true)
          : sqrt_b(std::sqrt(b)), gamma_0(gamma_0_),
          adapt_dim(adapt_dim_), large_jumps(large_jumps_)
      {}

        std::string name() const override
        {
          return "de-mc";
        }

        void propose(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active,
            const std::vector<Vector>& grad_log_pi, Vector& x_prop) const override
        {
          if (x.size() < 3)
            throw std::domain_error("de-mc kernel needs at least three states");

          if (adapt_dim)
            gamma = gamma_0 / std::sqrt(2*active.size());
          else
            gamma = gamma_0;
          if (large_jumps && rng<Real>.uniform_0_1() < 0.1)
            gamma = 1.;

          this->reservoir_sample(x.size(),2,i_chain,indices);

          x_prop = x[i_chain];
          for (const auto& comp : active)
            x_prop[comp] += gamma * (x[indices[0]][comp] - x[indices[1]][comp])
              + rng<Real>.normal(0.,sqrt_b);
        }

        Real log_hastings(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active, const Vector& x_prop,
            const std::vector<Vector>& grad_log_pi) const override
        {
          return 0.;
        }
    };

    /**
     * @brief Differential Evolution snooker proposal kernel
     */
    template<typename Vector>
      class DESnookerKernel : public TransitionKernel<Vector>
    {
      private:

        using typename TransitionKernel<Vector>::Real;

        Real gamma_s;

        mutable std::vector<std::size_t> indices;
        mutable Vector proj_0, proj_1;

      public:

        /**
         * @brief Constructor
         *
         * @param gamma_s_ DE snooker move step parameter
         */
        DESnookerKernel(Real gamma_s_ = 1.7)
          : gamma_s(gamma_s_)
        {}

        std::string name() const override
        {
          return "de-mc snooker";
        }

        void propose(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active,
            const std::vector<Vector>& grad_log_pi, Vector& x_prop) const override
        {
          if (x.size() < 4)
            throw std::domain_error("de-mc snooker kernel needs at least four states");

          this->reservoir_sample(x.size(),3,i_chain,indices);

          const Vector& x_0 = x[i_chain];
          const Vector& x_1 = x[indices[0]];
          project(x_0,x_1,active,x[indices[1]],proj_0);
          project(x_0,x_1,active,x[indices[2]],proj_1);

          x_prop = x_0;
          for (const auto& comp : active)
            x_prop[comp] += gamma_s * (proj_0[comp] - proj_1[comp]);
        }

        Real log_hastings(const std::vector<Vector>& x, std::size_t i_chain,
            const std::vector<std::size_t>& active, const Vector& x_prop,
            const std::vector<Vector>& grad_log_pi) const override
        {
          const Vector& x_0 = x[i_chain];
          const Vector& x_1 = x[indices[0]];

          Real norm_0 = 0.;
          Real norm_prop = 0.;

          for (std::size_t i = 0; i < x_0.dim(); i++)
            if (active[i])
            {
              norm_0 += (x_0[i] - x_1[i]) * (x_0[i] - x_1[i]);
              norm_prop += (x_prop[i] - x_1[i]) * (x_prop[i] - x_1[i]);
            }
          norm_0 = std::sqrt(norm_0);
          norm_prop = std::sqrt(norm_prop);

          return (active.size() - 1) * std::log(norm_prop/norm_0);
        }

      private:

        void project(const Vector& x_0, const Vector& x_1, const std::vector<std::size_t>& active,
            const Vector& x_2, Vector& x_proj) const
        {
          Real norm = 0.;
          Real scalar_prod = 0;

          for (const auto& comp : active)
          {
            const Real diff = x_1[comp] - x_0[comp];
            norm += diff * diff;
            scalar_prod += diff * (x_2[comp] - x_0[comp]);
          }
          norm = std::sqrt(norm);

          x_proj = x_0;
          for (const auto& comp : active)
            x_proj[comp] += scalar_prod/norm * (x_1[comp] - x_0[comp]);
        }

    };

  } // namespace DiffEvolution

} // namespace enSample

#endif // ENSAMPLE_KERNELS_HH
