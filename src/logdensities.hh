#ifndef ENSAMPLE_LOGDENSITIES_HH
#define ENSAMPLE_LOGDENSITIES_HH

#include<vector>

#ifdef HAVE_MUPARSER
#include "muParser.h"
#endif // HAVE_MUPARSER

namespace enSample {

  /**
   * @brief Default vector class implementation
   */
  template<typename R>
    class Vector : public std::vector<R>
  {
    public:

      /// @brief data type for components
      using Real = R;

      Vector()
        : std::vector<Real>()
      {}

      /**
       * @brief Constructor
       *
       * @param val vector components
       */
      template<typename... T>
        Vector(T... val)
        : std::vector<Real>({val...})
        {}

      /**
       * @brief Number of components
       *
       * @return current number of components
       */
      std::size_t dim() const
      {
        return std::vector<Real>::size();
      }

      /**
       * @brief Set number of components
       *
       * @param dim_ value to use
       */
      void setDim(std::size_t dim_)
      {
        std::vector<Real>::resize(dim_);
      }
  };

  /**
   * @brief Abstract base class for log-density functions
   */
  template<template<typename> class V, typename R>
    class LogDensity
    {
      public:

        /// @brief data type of chain states / positions
        using Vector = V<R>;
        /// @brief scalar data type of coordinates and values
        using Real   = R;

        LogDensity() {};
        virtual ~LogDensity() {};

        /**
         * @brief Name to distinguish log-densities in output
         */
        virtual std::string name() const = 0;

        /**
         * @brief Sequence of valid states (no repetitions allowed)
         */
        virtual void initialize(std::vector<Vector>& x) const = 0;

        /**
         * @brief Evaluate log-density value for given state
         */
        virtual Real value(const Vector& x) const = 0;

        /**
         * @brief Evaluate gradient of log-density for given state
         */
        virtual void gradient(const Vector& x, Vector& grad) const
        {
          // default: kernel doesn't provide gradient data
          throw std::logic_error("gradient not implemented, but requested by kernel");
        }
    };

#ifdef HAVE_MUPARSER
  /**
   * @brief log-density function based on runtime-evaluated expressions
   */
  template<template<typename> class V, typename R>
    class ParserDensity
    : public LogDensity<V,R>
    {
      public:

        using Vector = V<R>;
        using Real   = R;

      private:

        mu::Parser parser;
        std::map<std::string,Real*> var;
        mutable Vector x0;

      public:

        ParserDensity(const std::string& expression)
        {
          parser.SetVarFactory(add_variable,nullptr);
          parser.SetExpr(expression);
          std::cout << "expression: " << expression << std::endl;
          var = parser.GetUsedVar();
          x0.setDim(var.size());
        }

        std::string name() const override
        {
          return "muParser runtime density";
        }

        void initialize(std::vector<Vector>& x) const override
        {
          for (auto& vec : x)
          {
            vec.setDim(var.size());

            for (std::size_t i = 0; i < var.size(); i++)
              vec[i] = rng<Real>.uniform(1.,0.);
          }
        }

        Real value(const Vector& x) const override
        {
          if (x.dim() != var.size())
            throw std::domain_error("x and var have different dimensions");

          std::size_t count = 0;
          for (auto& entry : var)
            *(entry.second) = x[count++];

          Real out;
          try
          {
            out = parser.Eval();
          }
          catch(mu::Parser::exception_type& e)
          {
            std::cout << e.GetMsg() << std::endl;
            out = -std::numeric_limits<Real>::infinity();
          }

          return out;
        }

        void gradient(const Vector& x, Vector& grad) const override
        {
          const Real eps = 1e-6;
          x0 = x;

          for (std::size_t i = 0; i < x.dim(); i++)
          {
            x0[i] += eps;
            const Real plus = value(x0);
            x0[i] -= 2*eps;
            const Real minus = value(x0);
            x0[i] += eps;

            grad[i] = (plus - minus) / (2*eps);
          }
        }

      private:

        static double* add_variable(const char* str, void* ptr)
        {
          static Real var[128];
          static int index = -1;

          index++;

          std::cout << "Generating new variable \"" 
            << str << "\" (slots left: " 
            << 127-index << ")" << std::endl;

          var[index] = 0.;

          if (index > 127)
            throw mu::ParserError("Variable buffer overflow.");
          else
            return &var[index];
        }
    };
#endif // HAVE_MUPARSER

} // namespace enSample

#endif // ENSAMPLE_LOGDENSITIES_HH
