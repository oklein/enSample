#ifndef ENSAMPLE_RANDOM_HH
#define ENSAMPLE_RANDOM_HH

#ifdef USE_GSL_RNG
#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#else // USE_GSL_RNG
#include <random>
#endif // USE_GSL_RNG

namespace enSample {

#ifdef USE_GSL_RNG
  /**
   * @brief Pseudo-random number generator (GSL version)
   */
  template<typename Real>
    class RNG
    {
      gsl_rng* generator;

      public:

      RNG()
        : generator(gsl_rng_alloc(gsl_rng_taus))
      {}

      /**
       * @brief Initialize RNG state
       *
       * @param seed seed value to use
       */
      void seed(std::size_t seed)
      {
        gsl_rng_set(generator,seed);
      }

      /**
       * @brief Real uniform distribution on [0,1]
       *
       * @return sample of the distribution
       */
      Real uniform_0_1() const
      {
        return gsl_rng_uniform(generator);
      }

      /**
       * @brief Real uniform distribution on [a,b]
       *
       * @param a lower interval bound
       * @param b upper interval bound
       *
       * @return sample of the distribution
       */
      Real uniform(Real a, Real b) const
      {
        return a + uniform_0_1() * (b-a);
      }

      /**
       * @brief Standard normal distribution
       *
       * @return sample of the distribution
       */
      Real normal_0_1() const
      {
        return gsl_ran_gaussian(generator,1.);
      }

      /**
       * @brief Normal distribution with given mean and standard deviation
       *
       * @param mu mean of the distribution
       * @param sigma standard deviation
       *
       * @return sample of the distribution
       */
      Real normal(Real mu, Real sigma) const
      {
        return mu + normal_0_1() * sigma;
      }
    };
#else // USE_GSL_RNG
  /**
   * @brief Pseudo-random number generator (C++11 version)
   */
  template<typename Real>
    class RNG
    {
      mutable std::default_random_engine           generator;
      mutable std::uniform_real_distribution<Real> uniformDist;
      mutable std::normal_distribution<Real>       normalDist;

      public:

      RNG()
        : uniformDist(0.,1.), normalDist(0.,1.)
      {}

      /**
       * @brief Initialize RNG state
       *
       * @param seed seed value to use
       */
      void seed(std::size_t seed)
      {
        generator.seed(seed);
      }

      /**
       * @brief Real uniform distribution on [0,1]
       *
       * @return sample of the distribution
       */
      Real uniform_0_1() const
      {
        return uniformDist(generator);
      }

      /**
       * @brief Real uniform distribution on [a,b]
       *
       * @param a lower interval bound
       * @param b upper interval bound
       *
       * @return sample of the distribution
       */
      Real uniform(Real a, Real b) const
      {
        return a + uniform_0_1() * (b-a);
      }

      /**
       * @brief Standard normal distribution
       *
       * @return sample of the distribution
       */
      Real normal_0_1() const
      {
        return normalDist(generator);
      }

      /**
       * @brief Normal distribution with given mean and standard deviation
       *
       * @param mu mean of the distribution
       * @param sigma standard deviation
       *
       * @return sample of the distribution
       */
      Real normal(Real mu, Real sigma) const
      {
        return mu + normal_0_1() * sigma;
      }
    };
#endif // USE_GSL

  // instanciate an individual RNG for each type of number
  template<typename Real>
    RNG<Real> rng;

}

#endif // ENSAMPLE_RANDOM_HH
