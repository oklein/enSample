#include "../src/ensample.hh"

/**
 * A probability density function based on the Rosenbrock banana function
 */
template<template<typename> class V, typename R>
class Rosenbrock
: public enSample::LogDensity<V,R>
{
  public:

    using Vector = V<R>;
    using Real   = R;

  private:

    Real k;
    std::size_t dim;

  public:

    Rosenbrock(Real k_, std::size_t dim_)
      : k(k_), dim(dim_)
    {	
      if (dim % 2 != 0)
        throw(std::string("dim must be even"));
    }

    void initialize(std::vector<Vector>& x) const override
    {
      for (auto&& vec : x)
      {
        vec.setDim(dim);

        for (auto&& entry : vec)
          entry = enSample::rng<Real>.uniform_0_1();
      }
    }

    std::string name() const override
    {
      return "Rosenbrock function";
    }

    Real value(const Vector& x) const override
    {
      Real out = 0;
      for (std::size_t i = 0; i < dim; i += 2)
      {
        const Real first  = x[i+1] - x[i]*x[i];
        const Real second = 1. - x[i];
        out += 100. * first*first + second*second;
      }

      return -k * out;
    }

    void gradient(const Vector& x, Vector& grad) const override
    {
      for (std::size_t i = 0; i < dim; i += 2)
      {
        const Real first  = x[i+1] - x[i]*x[i];
        const Real second = 1. - x[i];
        grad[i] = -k * (- 400. * first * x[i] - 2. * second);
        grad[i+1] = -k * (200. * first);
      }
    }

};
