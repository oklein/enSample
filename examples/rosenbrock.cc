#include "rosenbrock.hh"

int main(int argc, char** argv)
{
  if (argc != 4)
  {
    std::cout << "Rosenbrock banana function example density" << std::endl;
    std::cout << "usage: ./rosenbrock <dimensions (even)> <components per step> <number of chains>" << std::endl;
    return 1;
  }

  std::size_t dim     = std::atoi(argv[1]);
  double      n_comp  = std::atof(argv[2]);
  std::size_t n_chain = std::atoi(argv[3]);

  std::size_t seed = time(nullptr);
  std::cout << "seed: " << seed << std::endl;
  enSample::rng<double>.seed(seed);

  using Density = Rosenbrock<enSample::Vector,double>;
  Density density(1./20.,dim);

  enSample::Sampler<Density> sampler(density,n_chain);

  sampler.set_twalk();
  sampler.set_target_active_components(n_comp);
  sampler.print_kernels();

  sampler.add_hook<enSample::FileHook>("rosenbrock.out",1000);
  sampler.simulate(10000000);
}

