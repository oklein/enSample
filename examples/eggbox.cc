#include "../src/ensample.hh"

/**
 * Eggbox example problem from Feroz et al. (2009)
 */
template<template<typename> class V, typename R>
class FerozEggbox
: public enSample::LogDensity<V,R>
{
  public:

    using Vector = V<R>;
    using Real   = R;

  private:

    Real k;
    std::size_t dim;

  public:

    FerozEggbox(Real k_ = 5., std::size_t dim_ = 2)
      : k(k_), dim(dim_)
    {	
      if (dim < 2)
        throw(std::string("dim must be at least two"));
    }

    void initialize(std::vector<Vector>& x) const override
    {
      for (auto&& vec : x)
      {
        vec.setDim(dim);

        for (auto&& entry : vec)
          entry = enSample::rng<Real>.uniform_0_1();
      }
    }

    std::string name() const override
    {
      return "eggbox";
    }

    Real value(const Vector& x) const override
    {
      Real out = 1.;
      for (std::size_t i = 0; i < dim; i++)
      {
        if (x[i] < 0. || x[i] > 1.)
          return - std::numeric_limits<Real>::infinity();

        out *= std::cos(k*M_PI*x[i]);
      }

      return std::pow(2. + out,5.);
    }

};

int main(int argc, char** argv)
{
  if (argc != 5)
  {
    std::cout << "Feroz et al.'s eggbox example with many local peaks" << std::endl;
    std::cout << "usage: ./eggbox <dimensions (even)> <components per step> <number of chains> <kernel(s)>" << std::endl;
    std::cout << "kernels: \"twalk\", \"gw\", \"de\" or \"metro\"" << std::endl;
    return 1;
  }

  const std::size_t dim     = std::atoi(argv[1]);
  const double      n_comp  = std::atof(argv[2]);
  const std::size_t n_chain = std::atoi(argv[3]);
  const std::string kernel  = argv[4];

  std::size_t seed = time(nullptr);
  std::cout << "seed: " << seed << std::endl;
  enSample::rng<double>.seed(seed);

  using Density = FerozEggbox<enSample::Vector,double>;
  Density density(5.,dim);

  enSample::Sampler<Density> sampler(density,n_chain);

  if (kernel == "twalk")
    sampler.set_twalk();
  else if (kernel == "gw")
    sampler.set_goodman_weare();
  else if (kernel == "de")
    sampler.set_differential_evolution();
  else if (kernel == "metro")
    sampler.add_kernel<enSample::MetropolisKernel>(1.,0.3);
  else
    throw std::logic_error("kernel choice not understood");

  sampler.set_target_active_components(n_comp);
  sampler.print_kernels();

  sampler.add_hook<enSample::FileHook>("eggbox.out",1000);

  sampler.simulate(1000000);
}

