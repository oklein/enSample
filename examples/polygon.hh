#ifndef ENSAMPLE_POLYGON_HH
#define ENSAMPLE_POLYGON_HH

#include<vector>
#include<limits>

#define USE_GNUPLOT
#include "../src/ensample.hh"

template<typename R>
class Polygon : public std::vector<R>
{
  public:

    using Real = R;

    Polygon()
    : std::vector<Real>()
    {}

    template<typename... T>
    Polygon(T... val)
      : std::vector<Real>({val...})
    {}

    /**
     * @brief degrees of freedom of polygon
     */
    std::size_t dim() const
    {
      return std::vector<Real>::size();
    }

    /**
     * @brief set degrees of freedom of polygon
     */
    void setDim(std::size_t dim_)
    {
      std::vector<Real>::resize(dim_);
    }

    /**
     * @brief 2D volume enclosed by polygon
     */
    Real area() const
    {
      Real out = 0.;

      for (std::size_t i = 0; i < dim(); i += 2)
        out += (*this)[i]*(*this)[(i+3)%dim()] - (*this)[(i+2)%dim()]*(*this)[i+1];

      return std::abs(0.5*out);
    }

    /**
     * @brief whether polygon is convex or not
     */
    bool is_convex() const
    {
      if (!is_simple())
        return false;

      const Real lastOr = orientation((*this)[dim()-2],(*this)[dim()-1],
          (*this)[0],(*this)[1],(*this)[2],(*this)[3]);

      for (std::size_t i = 0; i < dim(); i += 2)
      {
        const Real orient = orientation((*this)[i],(*this)[i+1],
            (*this)[(i+2)%dim()],(*this)[(i+3)%dim()],
            (*this)[(i+4)%dim()],(*this)[(i+5)%dim()]);
        if (lastOr * orient < -1e-10)
          return false;
      }

      return true;
    }

    /**
     * @brief whether polygon is simple (non-intersecting) or not
     */
    bool is_simple() const
    {
      if (dim() < 6)
        return false;

      for (std::size_t i = 0; i < dim(); i += 2)
        for (std::size_t j = i+2; j < dim(); j+= 2)
          if (crossing((*this)[i],(*this)[i+1],(*this)[(i+2)%dim()],
                (*this)[(i+3)%dim()],(*this)[j],(*this)[j+1],
                (*this)[(j+2)%dim()],(*this)[(j+3)%dim()]))
            return false;

      return true;
    }

    /**
     * @brief quotient of longest and shortest edge length
     */
    Real side_ratio() const
    {
      Real min = std::pow((*this)[dim()-2] - (*this)[0],2)
        + std::pow((*this)[dim()-1] - (*this)[1],2);
      Real max = min;

      for (std::size_t i = 0; i < dim() - 2; i += 2)
      {
        const Real distSquared = std::pow((*this)[i+2] - (*this)[i],2)
          + std::pow((*this)[i+3] - (*this)[i+1],2);
        if (distSquared < min)
          min = distSquared;
        if (distSquared > max)
          max = distSquared;
      }

      return std::sqrt(max / min);
    }

    /**
     * @brief quotient of longest and shortest node distance
     */
    Real distance_ratio() const
    {
      Real min = std::pow((*this)[dim()-2] - (*this)[0],2)
        + std::pow((*this)[dim()-1] - (*this)[1],2);
      Real max = min;

      for (std::size_t i = 0; i < dim(); i += 2)
        for (std::size_t j = i + 2; j < dim(); j += 2)
      {
        const Real distSquared = std::pow((*this)[i] - (*this)[j],2)
          + std::pow((*this)[i+1] - (*this)[j+1],2);
        if (distSquared < min)
          min = distSquared;
        if (distSquared > max)
          max = distSquared;
      }

      return std::sqrt(max / min);
    }

    template<typename Array>
      void plot(Array& data, Real& min_x, Real& max_x,
          Real& min_y, Real& max_y) const
      {
        data.resize(dim()/2+1);

        for (std::size_t i = 0; i < dim(); i += 2)
        {
          data[i/2] = {(*this)[i],(*this)[i+1]};
          if (min_x > (*this)[i])
            min_x = (*this)[i];
          if (max_x < (*this)[i])
            max_x = (*this)[i];
          if (min_y > (*this)[i+1])
            min_y = (*this)[i+1];
          if (max_y < (*this)[i+1])
            max_y = (*this)[i+1];
        }

        data[dim()/2] = {(*this)[0],(*this)[1]};
      }

  private:

    /**
     * @brief whether point c lies left of segment (a -> b), or right
     */
    Real orientation(Real a1, Real a2, Real b1, Real b2,
        Real c1, Real c2) const
    {
      // divides plane into positive (left) and negative (right) half planes
      return (b1 - a1) * (c2 - a2) - (c1 - a1) * (b2 - a2);
    }

    /**
     * @brief whether segments (a -> b) and (c -> d) share point
     */
    bool crossing(Real a1, Real a2, Real b1, Real b2,
        Real c1, Real c2, Real d1, Real d2) const
    {
      // (c -> d) is completely left resp. right of (a -> b)
      if (orientation(a1,a2,b1,b2,c1,c2) * orientation(a1,a2,b1,b2,d1,d2) >= -1e-10)
        return false;
      // same with switched roles
      if (orientation(c1,c2,d1,d2,a1,a2) * orientation(c1,c2,d1,d2,b1,b2) >= -1e-10)
        return false;

      // there must be a crossing
      return true;
    }
};

template<template<typename> class V, typename R>
class SimplePolygon
: public enSample::LogDensity<V,R>
{
  public:

    using Vector = V<R>;
    using Real   = R;

  private:

    Vector x_0, xp_0;

  public:

    SimplePolygon(std::size_t dim)
    {
      x_0.setDim(dim);
      xp_0.setDim(dim);
      for (std::size_t i = 0; i < dim; i += 2)
      {
        x_0[i]   = std::cos(i/Real(dim) * 2 * M_PI);
        x_0[i+1] = std::sin(i/Real(dim) * 2 * M_PI);
      }
      for (std::size_t i = 0; i < dim; i += 2)
      {
        xp_0[i]   = std::cos((i+0.5)/Real(dim) * 2 * M_PI);
        xp_0[i+1] = std::sin((i+0.5)/Real(dim) * 2 * M_PI);
      }
    }

    const Vector& Getx_0()  {return x_0;};
    const Vector& Getxp_0() {return xp_0;};

    std::string name() const override
    {
      return "Simple polygons";
    }

    Real value(const Vector& x) const override
    {
      if (!x.is_simple())
        return - std::numeric_limits<Real>::infinity();

      return - std::pow(1.-x.area(),2) - std::pow(1.-x.distance_ratio(),2);
    }

};

template<template<typename> class V, typename R>
class ConvexPolygon
: public enSample::LogDensity<V,R>
{
  public:

    using Vector = V<R>;
    using Real   = R;

  private:

    std::size_t dim;

    Real delta = M_PI;
    mutable std::size_t count = 0;

  public:

    ConvexPolygon(std::size_t dim_)
      : dim(dim_)
    {}

    void initialize(std::vector<Vector>& x) const override
    {
      for (auto& vec : x)
      {
        vec.setDim(dim);

        for (std::size_t i = 0; i < dim; i += 2)
        {
          vec[i]   = std::cos((i + count*delta)/dim * 2 * M_PI);
          vec[i+1] = std::sin((i + count*delta)/dim * 2 * M_PI);
        }
        count++;
      }
    }

    std::string name() const override
    {
      return "Convex polygons";
    }

    Real value(const Vector& x) const override
    {
      if (!x.is_convex())
        return - std::numeric_limits<Real>::infinity();

      return - std::pow(1.-x.area(),2) - std::pow(1.-x.distance_ratio(),2);
    }

};

#endif // ENSAMPLE_POLYGON_HH
