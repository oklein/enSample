#if (defined HAVE_BOOST && defined HAVE_GNUPLOT)
#include "rectangles.hh"

int main(int argc, char** argv)
{
  if (argc != 5)
  {
    std::cout << "simulation of a number of disjoint rectangles of various sizes" << std::endl;
    std::cout << "usage: ./rectangles <# rectangles> <components per step> <number of chains> <kernel(s)>" << std::endl;
    std::cout << "kernels: \"twalk\", \"gw\", \"de\" or \"metro\"" << std::endl;
    return 1;
  }

  const std::size_t rect    = std::atoi(argv[1]);
  const double      n_comp  = std::atof(argv[2]);
  const std::size_t n_chain = std::atoi(argv[3]);
  const std::string kernel  = argv[4];

  std::size_t seed = time(nullptr);
  std::cout << "seed: " << seed << std::endl;
  enSample::rng<double>.seed(seed);

  // disjoint rectangles, based on rectangles described as center + widths
  using Density = DisjointRectangles<Rectangles,double>;
  // alternatives:
  // implicitly flip "inverted" rectangles with negative widths
  //using Density = DisjointRectangles<Rectangles2,double>;
  // describe rectangles through corner points instead of center + widths
  //using Density = DisjointRectangles<Rectangles3,double>;
  Density density(rect*4);

  enSample::Sampler<Density> sampler(density,n_chain);

  if (kernel == "twalk")
    sampler.set_twalk();
  else if (kernel == "gw")
    sampler.set_goodman_weare();
  else if (kernel == "de")
    sampler.set_differential_evolution();
  else if (kernel == "metro")
    sampler.add_kernel<enSample::MetropolisKernel>(1.,0.5);
  else
    throw std::logic_error("kernel choice not understood");

  sampler.set_target_active_components(n_comp);
  sampler.print_kernels();

  sampler.add_hook<enSample::GnuplotHook>();

  sampler.simulate(100000);
}
#else // HAVE_BOOST && HAVE_GNUPLOT
#include <iostream>

int main()
{
  std::cout << "This example requires Boost, gnuplot and gnuplot-iostream" << std::endl;
  std::cout << "(see gnuplot-iostream.md for details)" << std::endl;
  return 1;
}
#endif // HAVE_BOOST && HAVE_GNUPLOT

