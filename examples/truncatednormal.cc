#include "truncatednormal.hh"

int main()
{
  // setup for random number generator
  std::size_t seed = time(nullptr);
  std::cout << "seed: " << seed << std::endl;
  enSample::rng<double>.seed(seed);

  // define underlying data type
  using enSample::Vector;
  using Real = double;

  // multivariate truncated Gaussian as per original implementation
  Vector<Real> m  = {2.0, 3.9, 5.2, 1.0, 7.8}; // mean
  Vector<Real> sd = {1.2, 2.1, 3.3, 0.1, 2.3}; // std dev
  Vector<Real> a  = {0.0, 0.0, 2.0, 0.0, 3.0}; // lower bounds

  // definition of objective function
  using LogDensity = TruncatedIndependentNormals<Vector,Real>;
  LogDensity logDensity(m, sd, a);

  // sampler object for simulations based on given density
  enSample::Sampler<LogDensity> sampler(logDensity);
  sampler.set_twalk();
  sampler.print_kernels();

  // add hook function that writes samples to file "ensample.out"
  sampler.add_hook<enSample::FileHook>();

  // run the sampler
  sampler.simulate(100000);
}

