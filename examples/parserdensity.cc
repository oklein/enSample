#ifdef HAVE_MUPARSER

#include "../src/ensample.hh"

int main(int argc, char** argv)
{
  if (argc != 2)
  {
    std::cout << "some possible expressions:\n"
      << "2D gaussian: \"-((x+y-3)^2+(2*(x-y))^2)\"\n"
      << "3D gaussian: \"-((x+y-3)^2+(2*(x-y))^2+(z-x-y)^2)\"\n"
      << "donut: \"-(sqrt(x^2+y^2)-2.6)^2/0.033\" (from Chi Feng's animation)\n"
      << "3D version: \"-(sqrt(x^2+y^2+z^2)-2.6)^2/0.033\"\n";
    return 0;
  }

  std::size_t seed = time(nullptr);
  std::cout << "seed: " << seed << std::endl;
  enSample::rng<double>.seed(seed);

  const std::string expression(argv[1]);

  using Density = enSample::ParserDensity<enSample::Vector,double>;
  Density density(expression);

  enSample::Sampler<Density> sampler(density);
  sampler.set_twalk();
  sampler.print_kernels();

  sampler.add_hook<enSample::FileHook>("parserdensity.out",1000);

  sampler.simulate(1000000);
}

#else // HAVE_MUPARSER
#include <iostream>

int main()
{
  std::cout << "this example requires the muParser library\n";
}
#endif // HAVE_MUPARSER
