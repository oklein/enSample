#ifndef ENSAMPLE_RECTANGLE_HH
#define ENSAMPLE_RECTANGLE_HH

#include<vector>
#include<limits>

#define USE_GNUPLOT
#include "../src/ensample.hh"

/**
 * @brief Class representing a collection of rectangles
 */
template<typename R>
class Rectangles : public std::vector<R>
{
  public:

    using Real = R;

    Rectangles()
      : std::vector<Real>()
    {}

    template<typename... T>
      Rectangles(T... val)
      : std::vector<Real>({val...})
      {}

    /**
     * @brief degrees of freedom of rectangles
     */
    std::size_t dim() const
    {
      return std::vector<Real>::size();
    }

    /**
     * @brief set degrees of freedom of rectangles
     */
    void setDim(std::size_t dim_)
    {
      std::vector<Real>::resize(dim_);
    }

    /**
     * @brief 2D volume enclosed by rectangles
     */
    Real area() const
    {
      Real out = 0.;

      for (std::size_t i = 0; i < dim(); i += 4)
        out += (*this)[i]*(*this)[i+1];

      return out;
    }

    /**
     * @brief quotient of longest and shortest edge length
     */
    Real side_ratio() const
    {
      Real min =   std::numeric_limits<Real>::max();
      Real max = - std::numeric_limits<Real>::max();

      for (std::size_t i = 0; i < dim(); i += 4)
      {
        const Real side1 = (*this)[i];
        if (side1 < min)
          min = side1;
        if (side1 > max)
          max = side1;

        const Real side2 = (*this)[i+1];
        if (side2 < min)
          min = side2;
        if (side2 > max)
          max = side2;
      }

      return max / min;
    }

    /**
     * @brief whether none of the rectangles cuts into another
     */
    bool are_disjoint() const
    {
      for (std::size_t i = 0; i < dim(); i += 4)
        for (std::size_t j = i+4; j < dim(); j+= 4)
          if (is_inside(i,j))
            return false;

      return true;
    }

    /**
     * @brief whether all lenghts/ widths are positive
     */
    bool are_rectangles() const
    {
      for (std::size_t i = 0; i < dim(); i +=4)
      {
        if ((*this)[i] <= 0)
          return false;
        if ((*this)[i+1] <= 0)
          return false;
      }

      return true;
    }

    /**
     * @brief whether all rectangles are inside bounds
     */
    bool are_inside(Real x_min, Real x_max, Real y_min, Real y_max) const
    {
      for (std::size_t i = 0; i < dim(); i += 4)
      {
        if ((*this)[i+2] - 0.5*(*this)[i] < x_min)
          return false;
        if ((*this)[i+2] + 0.5*(*this)[i] > x_max)
          return false;
        if ((*this)[i+3] - 0.5*(*this)[i+1] < y_min)
          return false;
        if ((*this)[i+3] + 0.5*(*this)[i+1] > y_max)
          return false;
      }

      return true;
    }

    template<typename Array>
      void plot(Array& data, Real& min_x, Real& max_x,
          Real& min_y, Real& max_y) const
      {
        for (std::size_t i = 0; i < dim(); i += 4)
        {
          data.emplace_back((*this)[i+2]-0.5*(*this)[i],(*this)[i+3]-0.5*(*this)[i+1]);
          data.emplace_back((*this)[i+2]+0.5*(*this)[i],(*this)[i+3]-0.5*(*this)[i+1]);
          data.emplace_back((*this)[i+2]+0.5*(*this)[i],(*this)[i+3]+0.5*(*this)[i+1]);
          data.emplace_back((*this)[i+2]-0.5*(*this)[i],(*this)[i+3]+0.5*(*this)[i+1]);
          data.emplace_back((*this)[i+2]-0.5*(*this)[i],(*this)[i+3]-0.5*(*this)[i+1]);
          data.emplace_back((*this)[i+2],std::numeric_limits<Real>::infinity());

          min_x = std::min(min_x,(*this)[i+2]-0.5*(*this)[i]);
          max_x = std::max(max_x,(*this)[i+2]+0.5*(*this)[i]);
          min_y = std::min(min_y,(*this)[i+3]-0.5*(*this)[i+1]);
          max_y = std::max(max_y,(*this)[i+3]+0.5*(*this)[i+1]);
        }
      }

    void initialize(std::size_t count)
    {
      for (std::size_t i = 0; i < dim(); i += 4)
      {
        (*this)[i]   = 8./dim();
        (*this)[i+1] = 8./dim();
        (*this)[i+2] = std::cos((i + count*M_PI)/dim() * 2 * M_PI);
        (*this)[i+3] = std::sin((i + count*M_PI)/dim() * 2 * M_PI);
      }
    }

  private:

    /**
     * @brief whether rectangle i crosses into rectangle j (and vice versa)
     */
    bool is_inside(std::size_t i, std::size_t j) const
    {
      if (std::abs((*this)[j+2] - (*this)[i+2]) < 0.5*((*this)[i] + (*this)[j])
          && std::abs((*this)[j+3] - (*this)[i+3]) < 0.5*((*this)[i+1] + (*this)[j+1]))
        return true;

      return false;
    }

};

/**
 * @brief Rectangles where negative widths are interpreted as positive
 */
template<typename R>
class Rectangles2 : public std::vector<R>
{
  public:

    using Real = R;

    Rectangles2()
      : std::vector<Real>()
    {}

    template<typename... T>
      Rectangles2(T... val)
      : std::vector<Real>({val...})
      {}

    /**
     * @brief degrees of freedom of rectangles
     */
    std::size_t dim() const
    {
      return std::vector<Real>::size();
    }

    /**
     * @brief set degrees of freedom of rectangles
     */
    void setDim(std::size_t dim_)
    {
      std::vector<Real>::resize(dim_);
    }

    /**
     * @brief 2D volume enclosed by rectangles
     */
    Real area() const
    {
      Real out = 0.;

      for (std::size_t i = 0; i < dim(); i += 4)
        out += std::abs((*this)[i]*(*this)[i+1]);

      return out;
    }

    /**
     * @brief quotient of longest and shortest edge length
     */
    Real side_ratio() const
    {
      Real min =   std::numeric_limits<Real>::max();
      Real max = - std::numeric_limits<Real>::max();

      for (std::size_t i = 0; i < dim(); i += 4)
      {
        const Real side1 = std::abs((*this)[i]);
        if (side1 < min)
          min = side1;
        if (side1 > max)
          max = side1;

        const Real side2 = std::abs((*this)[i+1]);
        if (side2 < min)
          min = side2;
        if (side2 > max)
          max = side2;
      }

      return max / min;
    }

    /**
     * @brief whether none of the rectangles cuts into another
     */
    bool are_disjoint() const
    {
      for (std::size_t i = 0; i < dim(); i += 4)
        for (std::size_t j = i+4; j < dim(); j+= 4)
          if (is_inside(i,j))
            return false;

      return true;
    }

    /**
     * @brief whether all lenghts/ widths are positive
     */
    bool are_rectangles() const
    {
      return true;
    }

    /**
     * @brief whether all rectangles are inside bounds
     */
    bool are_inside(Real x_min, Real x_max, Real y_min, Real y_max) const
    {
      for (std::size_t i = 0; i < dim(); i += 4)
      {
        if ((*this)[i+2] - 0.5*std::abs((*this)[i]) < x_min)
          return false;
        if ((*this)[i+2] + 0.5*std::abs((*this)[i]) > x_max)
          return false;
        if ((*this)[i+3] - 0.5*std::abs((*this)[i+1]) < y_min)
          return false;
        if ((*this)[i+3] + 0.5*std::abs((*this)[i+1]) > y_max)
          return false;
      }

      return true;
    }

    template<typename Array>
      void plot(Array& data, Real& min_x, Real& max_x,
          Real& min_y, Real& max_y) const
      {
        for (std::size_t i = 0; i < dim(); i += 4)
        {
          data.emplace_back((*this)[i+2]-0.5*(*this)[i],(*this)[i+3]-0.5*(*this)[i+1]);
          data.emplace_back((*this)[i+2]+0.5*(*this)[i],(*this)[i+3]-0.5*(*this)[i+1]);
          data.emplace_back((*this)[i+2]+0.5*(*this)[i],(*this)[i+3]+0.5*(*this)[i+1]);
          data.emplace_back((*this)[i+2]-0.5*(*this)[i],(*this)[i+3]+0.5*(*this)[i+1]);
          data.emplace_back((*this)[i+2]-0.5*(*this)[i],(*this)[i+3]-0.5*(*this)[i+1]);
          data.emplace_back((*this)[i+2],std::numeric_limits<Real>::infinity());

          min_x = std::min(min_x,(*this)[i+2]-0.5*(*this)[i]);
          max_x = std::max(max_x,(*this)[i+2]+0.5*(*this)[i]);
          min_y = std::min(min_y,(*this)[i+3]-0.5*(*this)[i+1]);
          max_y = std::max(max_y,(*this)[i+3]+0.5*(*this)[i+1]);
        }
      }

    void initialize(std::size_t count)
    {
      for (std::size_t i = 0; i < dim(); i += 4)
      {
        (*this)[i]   = 1./std::sqrt(dim());
        (*this)[i+1] = 1./std::sqrt(dim());
        (*this)[i+2] = std::cos((i + count*M_PI)/dim() * 2 * M_PI);
        (*this)[i+3] = std::sin((i + count*M_PI)/dim() * 2 * M_PI);
      }
    }

  private:

    /**
     * @brief whether rectangle i crosses into rectangle j (and vice versa)
     */
    bool is_inside(std::size_t i, std::size_t j) const
    {
      if (std::abs((*this)[j+2] - (*this)[i+2])
          < 0.5*(std::abs((*this)[i]) + std::abs((*this)[j]))
          && std::abs((*this)[j+3] - (*this)[i+3])
          < 0.5*(std::abs((*this)[i+1]) + std::abs((*this)[j+1])))
        return true;

      return false;
    }

};

/**
 * @brief Rectangles defined by lower left and upper right corner
 */
template<typename R>
class Rectangles3 : public std::vector<R>
{
  public:

    using Real = R;

    Rectangles3()
      : std::vector<Real>()
    {}

    template<typename... T>
      Rectangles3(T... val)
      : std::vector<Real>({val...})
      {}

    /**
     * @brief degrees of freedom of rectangles
     */
    std::size_t dim() const
    {
      return std::vector<Real>::size();
    }

    /**
     * @brief set degrees of freedom of rectangles
     */
    void setDim(std::size_t dim_)
    {
      std::vector<Real>::resize(dim_);
    }

    /**
     * @brief 2D volume enclosed by rectangles
     */
    Real area() const
    {
      Real out = 0.;

      for (std::size_t i = 0; i < dim(); i += 4)
        out += ((*this)[i+1]-(*this)[i]) * ((*this)[i+3]-(*this)[i+2]);

      return out;
    }

    /**
     * @brief quotient of longest and shortest edge length
     */
    Real side_ratio() const
    {
      Real min =   std::numeric_limits<Real>::max();
      Real max = - std::numeric_limits<Real>::max();

      for (std::size_t i = 0; i < dim(); i += 4)
      {
        const Real side1 = (*this)[i+1]-(*this)[i];
        if (side1 < min)
          min = side1;
        if (side1 > max)
          max = side1;

        const Real side2 = (*this)[i+3]-(*this)[i+2];
        if (side2 < min)
          min = side2;
        if (side2 > max)
          max = side2;
      }

      return max / min;
    }

    /**
     * @brief whether none of the rectangles cuts into another
     */
    bool are_disjoint() const
    {
      for (std::size_t i = 0; i < dim(); i += 4)
        for (std::size_t j = i+4; j < dim(); j+= 4)
          if (is_inside(i,j))
            return false;

      return true;
    }

    /**
     * @brief whether all lenghts/ widths are positive
     */
    bool are_rectangles() const
    {
      for (std::size_t i = 0; i < dim(); i +=4)
      {
        if ((*this)[i+1]-(*this)[i] <= 0)
          return false;
        if ((*this)[i+3]-(*this)[i+2] <= 0)
          return false;
      }

      return true;
    }

    /**
     * @brief whether all rectangles are inside bounds
     */
    bool are_inside(Real x_min, Real x_max, Real y_min, Real y_max) const
    {
      for (std::size_t i = 0; i < dim(); i += 4)
      {
        if ((*this)[i] < x_min)
          return false;
        if ((*this)[i+1] > x_max)
          return false;
        if ((*this)[i+2] < y_min)
          return false;
        if ((*this)[i+3] > y_max)
          return false;
      }

      return true;
    }

    template<typename Array>
      void plot(Array& data, Real& min_x, Real& max_x,
          Real& min_y, Real& max_y) const
      {
        for (std::size_t i = 0; i < dim(); i += 4)
        {
        data.emplace_back((*this)[i]  ,(*this)[i+2]);
        data.emplace_back((*this)[i+1],(*this)[i+2]);
        data.emplace_back((*this)[i+1],(*this)[i+3]);
        data.emplace_back((*this)[i]  ,(*this)[i+3]);
        data.emplace_back((*this)[i]  ,(*this)[i+2]);
        data.emplace_back((*this)[i+2],std::numeric_limits<Real>::infinity());

        min_x = std::min(min_x,(*this)[i]);
        max_x = std::max(max_x,(*this)[i+1]);
        min_y = std::min(min_y,(*this)[i+2]);
        max_y = std::max(max_y,(*this)[i+3]);
        }
      }

    void initialize(std::size_t count)
    {
      for (std::size_t i = 0; i < dim(); i += 4)
      {
        (*this)[i]   = std::cos((i + count*M_PI)/dim() * 2 * M_PI) - 0.5/std::sqrt(dim());
        (*this)[i+1] = std::cos((i + count*M_PI)/dim() * 2 * M_PI) + 0.5/std::sqrt(dim());
        (*this)[i+2] = std::sin((i + count*M_PI)/dim() * 2 * M_PI) - 0.5/std::sqrt(dim());
        (*this)[i+3] = std::sin((i + count*M_PI)/dim() * 2 * M_PI) + 0.5/std::sqrt(dim());
      }
    }

  private:

    /**
     * @brief whether rectangle i crosses into rectangle j (and vice versa)
     */
    bool is_inside(std::size_t i, std::size_t j) const
    {
      if ((*this)[j+1] > (*this)[i]  && (*this)[i+1] > (*this)[j]
          && (*this)[j+3] > (*this)[i+2] && (*this)[i+3] > (*this)[j+2])
        return true;

      return false;
    }

};

template<template<typename R> class V, typename R>
class DisjointRectangles
: public enSample::LogDensity<V,R>
{
  public:

    using Vector = V<R>;
    using Real   = R;

  private:

    std::size_t dim;

    Real delta = M_PI;
    mutable std::size_t count = 0;

  public:

    DisjointRectangles(std::size_t dim_)
      : dim(dim_)
    {}

    void initialize(std::vector<Vector>& x) const override
    {
      for (auto& vec : x)
      {
        vec.setDim(dim);
        vec.initialize(count++);
      }
    }

    std::string name() const override
    {
      return "Disjoint rectangles";
    }

    Real value(const Vector& x) const override
    {
      if (!x.are_rectangles() || !x.are_disjoint() || !x.are_inside(-5.,5.,-5.,5.))
        return - std::numeric_limits<Real>::infinity();

      return - std::pow(x.dim()-x.area(),2) - std::pow(1.-x.side_ratio(),2);
    }

};

#endif // ENSAMPLE_RECTANGLE_HH
