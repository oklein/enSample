#ifndef ENSAMPLE_TRUNCATEDNORMAL_HH
#define ENSAMPLE_TRUNCATEDNORMAL_HH

#include<limits>

#include "../src/ensample.hh"

template<template<typename> class V, typename R>
class TruncatedIndependentNormals
: public enSample::LogDensity<V,R>
{
  public:

    using Vector = V<R>;
    using Real   = R;

  private:

    Vector m;  // means of normal distributions
    Vector sd; // standard deviations of normal distributions
    Vector a;  // lower boundary for the support of each normal distribution

  public:

    TruncatedIndependentNormals(const Vector& m_, const Vector& sd_, const Vector& a_)
      : m(m_), sd(sd_), a(a_)
    {	
      if (m.dim() != sd.dim() || sd.dim() != a.dim())
        throw std::domain_error("m, sd and a must have consistent dimensions");
    }

    std::string name() const override
    {
      return "Truncated independent normals";
    }

    void initialize(std::vector<Vector>& x) const override
    {
      for (auto& vec : x)
      {
        vec.setDim(m.dim());

        for (std::size_t i = 0; i < vec.dim(); i++)
          vec[i]  = enSample::rng<Real>.uniform(a[i], m[i]);
      }
    }

    Real value(const Vector& x) const override
    {
      if (x.dim() != a.dim())
        throw std::domain_error("x and a have different dimensions");

      Real out = 0;
      for (std::size_t i = 0; i < x.dim(); i++)
      {
        if (x[i] < a[i])
          return -std::numeric_limits<Real>::infinity();

        const Real val = (x[i] - m[i])/sd[i];
        out += -0.5 * val * val;
      }

      return out;
    }

};

#endif // ENSAMPLE_TRUNCATEDNORMAL_HH
