#include "../src/ensample.hh"

/**
 * A probability density function based on Neal's slice sampling example
 */
template<template<typename> class V, typename R>
class NealFunnel
: public enSample::LogDensity<V,R>
{
  public:

    using Vector = V<R>;
    using Real   = R;

  private:

    Real k;
    std::size_t dim;
    const Real log_sqrt_two_pi;
    mutable std::size_t count = 0;

  public:

    NealFunnel(Real k_ = 3., std::size_t dim_ = 10)
      : k(k_), dim(dim_), log_sqrt_two_pi(std::log(std::sqrt(2*M_PI)))
    {	
      if (dim < 2)
        throw(std::string("dim must be at least two"));
    }

    void initialize(std::vector<Vector>& x) const override
    {
      for (auto&& vec : x)
      {
        vec.setDim(dim);

        vec[0] = 1. + std::sin(count);
        for (std::size_t j = 1; j < dim; j++)
          vec[j] = 0. + std::sin(count);
        count++;
      }
    }

    std::string name() const override
    {
      return "Neal's funnel example density";
    }

    Real value(const Vector& x) const override
    {
      Real out = x[0]*x[0] / (k*k);
      const Real& variance = std::exp(x[0]);

      for (std::size_t i = 1; i < dim; i++)
        out += x[i]*x[i] / variance;
      //out += (dim-1) * std::log(std::sqrt(2.*M_PI*variance));
      out += (dim-1) * (log_sqrt_two_pi + 0.5 * x[0]);

      return - out;
    }

    void gradient(const Vector& x, Vector& grad) const override
    {
      const Real& variance = std::exp(x[0]);

      grad[0] = 2. * x[0] / (k*k) + (dim-1) * 0.5 * x[0];

      for (std::size_t i = 1; i < dim; i++)
      {
        grad[0] -= x[i]*x[i] / variance;
        grad[i] = 2. * x[i] / variance;
      }
    }

};

int main(int argc, char** argv)
{
  if (argc != 5)
  {
    std::cout << "Neal's funnel example for hierarchical models and sampling bias" << std::endl;
    std::cout << "usage: ./funnel <dimensions (>= 2)> <components per step> <number of chains> <kernel(s)>" << std::endl;
    std::cout << "kernels: \"twalk\", \"gw\", \"de\", \"metro\" or \"mala\"" << std::endl;
    return 1;
  }

  const std::size_t dim     = std::atoi(argv[1]);
  const double      n_comp  = std::atof(argv[2]);
  const std::size_t n_chain = std::atoi(argv[3]);
  const std::string kernel  = argv[4];

  std::size_t seed = time(nullptr);
  std::cout << "seed: " << seed << std::endl;
  enSample::rng<double>.seed(seed);

  using Density = NealFunnel<enSample::Vector,double>;
  Density density(3.,dim);

  enSample::Sampler<Density> sampler(density,n_chain);

  if (kernel == "twalk")
    sampler.set_twalk();
  else if (kernel == "gw")
    sampler.set_goodman_weare();
  else if (kernel == "de")
    sampler.set_differential_evolution();
  else if (kernel == "metro")
    sampler.add_kernel<enSample::MetropolisKernel>(1.,0.5);
  else if (kernel == "mala")
    sampler.add_kernel<enSample::MALAKernel>(1.,0.01);
  else
    throw std::logic_error("kernel choice not understood");

  sampler.set_target_active_components(n_comp);
  sampler.print_kernels();

  sampler.add_hook<enSample::FileHook>("funnel.out",1000);

  sampler.simulate(1000000);
}

