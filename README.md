enSample: The C++ Multi-Chain MCMC Sampler
==========================================

enSample is a flexible and efficient implementation of
several multi-chain Markov Chain Monte Carlo (MCMC) algorithms:

1. J.A. Christen's and C. Fox's scale-invariant [t-walk MCMC
    algorithm][1], a "general purpose sampling algorithm for continuous
    distributions" (DOI: [10.1214/10-BA603][2])
2. J. Goodman's and J. Weare's affine-invariant Ensemble Sampler
    (DOI: [10.2140/camcos.2010.5.65][3])
3. C.J.F. Ter Braak's Differential Evolution MCMC
    (DOI: [10.1007/s11222-006-8769-1][4])

The package includes a flexible plugin system, which can be used to
incorporate other transition kernels and sampling hooks, with a
classical Metropolis kernel and a Metropolis-adjusted Langevin
(MALA) kernel already included as part of the implementation.

The implementation currently provides the following features:
* target log-density functions can be supplied via small C++ classes
  or as a mathematical expression that is parsed at runtime
* support for user-supplied transition kernels, sample hooks, and
  custom vector classes
* arbitrary number of chains, from single-chain to large ensembles
* arbitrary number of active components per MCMC step, if desired


Copying and Contributing
========================

enSample is free software under the BSD 3-clause license, and its
documentation is licensed under a Creative Commons license. See
[LICENSE.md](./LICENSE.md) for details.

If you would like to contribute, please feel free to open a merge
request. It would of course be nice if your code would follow the
style conventions already present in the source, but there are
currently no strict guidelines beyond that. You are invited to use
the bug tracker of this repository if you find any errors or mistakes,
and you may also contact the author directly
(ole.klein@iwr.uni-heidelberg.de) if there is something you would like
to discuss in detail.


Dependencies
============

The package does not have any hard dependencies, except for a
not-too-ancient C++ compiler (C++17 and above), a reasonably new
CMake installation (3.10 and above), and a version of make.

Optional Dependencies
---------------------

* The optional real-time visualization of samples is based on
    [gnuplot-iostream][5], which requires the Boost C++ libraries
    and gnuplot to be installed.
* The optional log-density definition parser requires [muParser][6].
* Building the automatic documentation requires Doxygen.


Installation
============

enSample supports three different ways of installation:
1. Using CMake for configuration and installation
2. Installation as a submodule of the [DUNE framework][7]
3. A barebones header-only approach

Building with CMake (Default)
-----------------------------

The standard way of building enSample is via CMake. Create a build
directory, either as a subfolder within the source directory tree,
or in some other location, and then call CMake to configure the
project and build it with make:

```bash
# create build directory
mkdir build
cd build

# run CMake for the build directory
# replace with path to source if using custom location
cmake ..

# optional: modify configuration (see below)
ccmake .

# build the project
make

# optional: install header files (see below)
make install
```

CMake will try to detect optional dependencies and generate an
initial configuration. Calling `ccmake .` within the build directory,
you can modify this configuration to your liking. Note that some
distros don't install ccmake with make, but provide it as a separate
package instead. You can then either install ccmake directly, or set
the following options noninteractively by using the `-D` commandline
option when calling cmake as described above.

Enable / disable optional parts of the project:
* `BUILD_DOCUMENTATION` toggles whether or not Doxygen documentation
    is generated within the `doc/` subfolder. If you build documentation,
    you may access it by opening `doc/html/index.html` in a browser of
    your choice.
* `BUILD_EXAMPLES` toggles whether the toy problems within the
    `examples` folder are built. These may be a good starting point for
    your own projects. Note that some examples require additional
    software to be installed, e.g., the `parserdensity` example uses a
    mathematical expression of your choice as log-density to sample from,
    and is based on muParser. Some of the examples provide visualization
    and require Boost and gnuplot, see above.
* `BUILD_TESTING` enables the compilation of tests within the `test/`
    directory. You can run `ctest` to run these tests and sanity checks.

Configure the CMake build process:
* `CMAKE_BUILD_TYPE` is used to choose compilation options. The default
    of CMake is an unoptimized `Debug` build, which is helpful when you
    encounter bugs while writing your own application. Be sure to switch
    this over to `Release` to turn optimization on once everything works
    as expected.
* `CMAKE_INSTALL_PREFIX` specifies the directory where the project should
    be installed if you type `make install`.
* `CMAKE_PREFIX_PATH` can be used to guide the build system when you
    have installed software in non-standard locations and would like to
    make it available.

Enable optional features:
* `USE_GNUPLOT_VIZ` enables the real-time visualization using
    gnuplot-iostream if the package and its dependencies (Boost and
    gnuplot) were found by CMake. See its [README](./gnuplot-iostream.md)
    for details.
* `USE_GSL_RNG` enables the GNU Scientific Library pseudo-random number
    generator if the GSL was found by CMake.

Using `make install`, the header files in the `src/` directory can be
copied to the subdirectory `include/enSample/` of the path specified by
the `CMAKE_INSTALL_PREFIX` variable. This step is entirely optional.
Note that the example and test executables are not installed by this
command.


Integration into DUNE
---------------------

enSample can be built and used as a DUNE module, with the DUNE
installer handling dependencies. The only other required
module is [dune-common][8]. Simply place the source next to
your other DUNE modules, and let the `dunecontrol` script take care of
things. Here is an example options file:

```bash
# subdirectory to use as build-directory
BUILDDIR="$HOME/dune/build"
 
# paths to external software in non-default locations
CMAKE_PREFIX_PATH="$HOME/software"
 
GXX_WARNING_OPTS="-Wall"
 
GXX_OPTS="-march=native -g -O2 -std=c++20"
 
# set flags for CMake:
# compiler flags, 3rd party software, grid extensions
CMAKE_FLAGS=" \
-DCMAKE_Fortran_COMPILER='gfortran-10.2.0' \
-DCMAKE_C_COMPILER='gcc-10.2.0' \
-DCMAKE_CXX_COMPILER='g++-10.2.0' \
-DCMAKE_CXX_FLAGS='$GXX_WARNING_OPTS $GXX_OPTS' \
-DDUNE_SYMLINK_TO_SOURCE_TREE=1 \
"
```

Most of these options are not really necessary, they are there to
show you how DUNE can be configured. Have a look at the DUNE
webpage if you want to know more, and make sure to either
adapt or remove the compiler specifications above. Here is how you
build the project using an options file called `myopts`:

```bash
dune-common/bin/dunecontrol --opts=myopts --module=enSample
```

Header Only Approach
--------------------

Except for the examples and automatic tests contained in their
respective directories, enSample does not contain any libraries
or executables. It is therefore straight-forward to ``install''
the package by copying the contents of the `src` folder to a
destination that is searched by the compiler for header files,
or to a folder that is then made known to the compiler through
an appropriate flag, e.g., `-I/path/to/headers` or similar.

As a special case, the repository can be incorporated into
another project as a git submodule.


Examples
========

The `examples` folder contains a number of toy problems that
illustrate how enSample can be used to sample from different
target densities.

Included are:

* `eggbox`: Eggbox example problem from Feroz et al. (2009),
    with many isolated modes
* `funnel`: The Slice Sampling example problem from Neal (2003)
* `parserdensity`: An example application for the parser-based
    log-density, requires muParser
* `polygon`: Generates convex polygons. An alternative class
    is provided that generates arbitrary (not necessarily convex)
    polygons.
* `rectangles`: Generates a number of non-overlapping axiparallel
    rectangles. Three different implementation classes are
    available.
* `rosenbrock`: Implementation of the well-known Rosenbrock
    banana function as log-density
* `truncatednormal`: The example application of the original
    t-walk implementation, sampling from a truncated multivariate
    normal distribution


Quick Start Instructions
========================

This is a description of the general workflow for the MCMC
samplers provided by the package. A large part of these steps
is also demonstrated within the example files mentioned above.
Have a look at the automatic documentation if you would like
to know more about the class informations and definitions.

Specification of Target Density
-------------------------------

* If the logarithm of your density is a simple mathematical
    expression, consider using the `parserdensity` example
    program, or basing your code on the `ParserDensity` C++
    class. This way, you don't need to write any custom methods.
* In other cases, write a custom C++ class that provides the
    necessary methods, most importantly a `value` method that
    evaluates your log-density function. The `gradient` method
    is only needed if you intend to use MALA or a similar kernel.

Choosing a Set of Transition Kernels
------------------------------------

* Call `set_twalk()` on the sampler to configure it as a
    t-walk scale-invariant MCMC method. Call `set_goodman_weare()`
    instead to configure it as an affine-invariant Ensemble Sampler,
    and `set_differential_evolution()` for a sampler based on
    Differential Evolution.
* You can mix-and-match different kernels by calling the
    `add_kernel(...)` methods and supplying a scalar weight
    (specifying how often this kernel should be chosen relative
    to others). This can also be used to supply custom kernel
    classes.

Hooks for Accessing / Using Produced Samples
--------------------------------------------

* A set of hook classes for common tasks is provided and can be
    installed using the `add_hook(...)` method, e.g., printing
    the samples on screen, writing them to a file, displaying
    them in real time using gnuplot, or similar.
* Custom hook classes can be used to provide additional
    functionality, and multiple hooks can be used at the same time.
    Each such hook provides a callback function, and enSample
    controls the program flow and hands samples over to each hook.
    See below for an alternative where enSample is used by calling
    code instead.

Generation of Samples
---------------------

* Simply call the `simulate(...)` method with the desired number of
    samples. The sampler draws samples from the target log-density,
    alternating between the different chains and kernels, and calls
    the installed hooks for each such sample.
* If you need more control over the sampling process, e.g., when
    using enSample within a larger simulation framework, you may
    call `simulate(0)` to set up the sampler without actually
    generating any samples. Then, call the `step()` method to
    generate a single sample, which is returned as a reference by
    the method, use it within your framework, and call `step()`
    again when you need the next sample.

[1]: https://www.cimat.mx/~jac/twalk/
[2]: https://dx.doi.org/10.1214/10-BA603
[3]: https://dx.doi.org/10.2140/camcos.2010.5.65
[4]: https://dx.doi.org/10.1007/s11222-006-8769-1
[5]: https://github.com/dstahlke/gnuplot-iostream
[6]: https://github.com/beltoforion/muparser
[7]: https://dune-project.org
[8]: https://gitlab.dune-project.org/core/dune-common
