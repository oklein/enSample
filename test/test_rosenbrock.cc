#include "../examples/rosenbrock.hh"

int main()
{
  std::size_t dim     = 2;
  double      n_comp  = 2;
  std::size_t n_chain = 2;

  std::size_t seed = time(nullptr);
  enSample::rng<double>.seed(seed);

  using Density = Rosenbrock<enSample::Vector,double>;
  Density density(1./20.,dim);

  enSample::Sampler<Density> sampler(density,n_chain);

  sampler.set_twalk();
  sampler.set_target_active_components(n_comp);
  sampler.print_kernels();

  sampler.simulate(100000);

  return 0;
}

