#include "../src/ensample.hh"

#include "mockdensity.hh"

int main()
{
  using Density = MockDensity<enSample::Vector,double>;
  Density density;

  enSample::Sampler<Density> sampler(density,1);

  // sanity checks for classic metropolis random walk kernel
  if (sampler.num_kernels() != 0)
    return 1;
  sampler.add_kernel<enSample::MetropolisKernel>(1.);
  if (sampler.num_kernels() != 1)
    return 1;

  {
    auto [name, weight, count, acc] = sampler.kernel_info(0);
    if (name != "metropolis" || weight != 1. || count != 0 || acc != 0)
      return 1;
  }

  sampler.clear_kernels();
  if (sampler.num_kernels() != 0)
    return 1;

  // sanity checks for t-walk kernels
  sampler.set_twalk();
  if (sampler.num_kernels() != 4)
    return 1;

  {
    std::vector<std::string> names
      = {"t-walk walk", "t-walk traverse", "t-walk hop", "t-walk blow"};
    std::vector<double> weights = {60./122., 60./122., 1./122., 1./122.};

    for (std::size_t i = 0; i < 4; i++)
    {
      const auto [name, weight, count, acc] = sampler.kernel_info(i);
      if (name != names[i] || weight != weights[i] ||  count != 0 || acc != 0)
        return 1;
    }
  }

  // sanity checks for ensemble sampler
  sampler.set_goodman_weare();
  if (sampler.num_kernels() != 1)
    return 1;

  {
    auto [name, weight, count, acc] = sampler.kernel_info(0);
    std::cout << weight << " " << name << " " << count << " " << acc << std::endl;
    if (name != "G&W walk" || weight != 1. ||  count != 0 || acc != 0)
      return 1;
  }

  // sanity checks for differential evolution
  sampler.set_differential_evolution();
  if (sampler.num_kernels() != 2)
    return 1;

  {
    std::vector<std::string> names
      = {"de-mc", "de-mc snooker"};
    std::vector<double> weights = {0.9, 0.1};

    for (std::size_t i = 0; i < 2; i++)
    {
      const auto [name, weight, count, acc] = sampler.kernel_info(i);
      if (name != names[i] || weight != weights[i] ||  count != 0 || acc != 0)
        return 1;
    }
  }


  return 0;
}

