#include "../src/ensample.hh"

#include "mockdensity.hh"

int main()
{
  using Density = MockDensity<enSample::Vector,double>;
  Density density;

  enSample::Sampler<Density> sampler(density,1);

  if (sampler.num_hooks() != 0)
    return 1;
  sampler.add_hook<enSample::EchoHook>();
  if (sampler.num_hooks() != 1)
    return 1;

  try
  {
    const auto& hook
      = dynamic_cast<const enSample::EchoHook<enSample::Vector<double>>&>
      (sampler.hook(0));
    if (hook.name() != "echo")
      return 1;
  }
  catch(...)
  {
    return 1;
  }

  sampler.add_hook<enSample::MeanHook>();

  if (sampler.num_hooks() != 2)
    return 1;
  try
  {
    const auto& hook
      = dynamic_cast<const enSample::MeanHook<enSample::Vector<double>>&>
      (sampler.hook(1));
    if (hook.name() != "mean")
      return 1;
  }
  catch(...)
  {
    return 1;
  }

  sampler.clear_hooks();
  if (sampler.num_hooks() != 0)
    return 1;

  return 0;
}

