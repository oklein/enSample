#include "../src/ensample.hh"

#include "mockdensity.hh"

int main()
{
  using Density = MockDensity<enSample::Vector,double>;
  Density density;

  enSample::Sampler<Density> sampler(density,2);

  sampler.set_twalk();

  // check if total / accepted steps statistics match
  std::size_t steps = 100000;
  sampler.simulate(steps);
  const auto& stats = sampler.provide_statistics();
  std::size_t acc_steps = stats[0][1];

  std::size_t count1 = 0, count2 = 0, acc1 = 0, acc2 = 0;
  for (std::size_t i = 0; i < 4; i++)
  {
    const auto [name, weight, count, acc] = sampler.kernel_info(i);
    count1 += count;
    acc1 += acc;

    count2 += stats[i+1][0];
    acc2 += stats[i+1][1];
  }
  if (count1 != steps || count2 != steps
      || acc1 != acc_steps || acc2 != acc_steps)
    return 1;

  // check if statistics match when calling step() manually
  for (std::size_t k = 1; k <= steps; k++)
  {
    sampler.step();
    const auto& stats = sampler.provide_statistics();
    std::size_t num_steps = stats[0][0];
    std::size_t acc_steps = stats[0][1];

    std::size_t count1 = 0, count2 = 0, acc1 = 0, acc2 = 0;
    for (std::size_t i = 0; i < 4; i++)
    {
      const auto [name, weight, count, acc] = sampler.kernel_info(i);
      count1 += count;
      acc1 += acc;

      count2 += stats[i+1][0];
      acc2 += stats[i+1][1];
    }
    if (num_steps != steps + k || count1 != num_steps || count2 != num_steps
        || acc1 != acc_steps || acc2 != acc_steps)
      return 1;
  }



  return 0;
}

