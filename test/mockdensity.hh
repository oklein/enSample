#ifndef ENSAMPLE_TEST_MOCKDENSITY_HH
#define ENSAMPLE_TEST_MOCKDENSITY_HH

#include "../src/ensample.hh"

/**
 * A mock density function for tests (1D constant function)
 */
template<template<typename> class V, typename R>
class MockDensity
: public enSample::LogDensity<V,R>
{
  public:

    using Vector = V<R>;
    using Real   = R;

    MockDensity() {}

    void initialize(std::vector<Vector>& x) const override
    {
      for (std::size_t i = 0; i < x.size(); i++)
      {
        x[i].setDim(1);
        x[i][0] = std::pow(-1,i) * i;
      }
    }

    std::string name() const override
    {
      return "mock density for testing";
    }

    Real value(const Vector& x) const override
    {
      return 0.;
    }

};

#endif // ENSAMPLE_MOCKDENSITY_HH
