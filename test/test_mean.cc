#include "../examples/truncatednormal.hh"

// define underlying data type
using enSample::Vector;
using Real = double;

  template<typename Sampler>
bool check_mean(const Sampler& sampler, const Vector<Real>& m,
    const Vector<Real>& sd, Real tol_m, Real tol_sd,
    const std::initializer_list<std::size_t>& indices)
{
  const auto& mean_hook = dynamic_cast<const enSample::MeanHook<Vector<Real>>&>
    (sampler.hook(0));
  const auto& mean = mean_hook.get_mean();
  const auto& square_sum = mean_hook.get_square_sum();
  std::size_t count = mean_hook.get_count();

  for (auto i : indices)
  {
    const Real std_dev = std::sqrt(square_sum[i]/(count-1));
    std::cout << "[" << m[i] << " , " << mean[i]
      << "] : [" << sd[i] << " , " << std_dev << "]" << std::endl;
    if (mean[i] < m[i] - tol_m || mean[i] > m[i] + tol_m)
    {
      std::cout << i << " mean" << std::endl;
      return true;
    }
    if (std_dev < sd[i] - tol_sd || std_dev > sd[i] + tol_sd)
    {
      std::cout << i << " std_dev" << std::endl;
      return true;
    }
  }

  return false;
}

int main()
{
  std::size_t seed = time(nullptr);
  enSample::rng<double>.seed(seed);

  // multivariate truncated Gaussian as per original implementation
  Vector<Real> m  = {2.0, 3.9, 5.2, 1.0, 7.8}; // mean
  Vector<Real> sd = {1.2, 2.1, 3.3, 0.1, 2.3}; // std dev
  Vector<Real> a  = {0.0, 0.0, 2.0, 0.0, 3.0}; // lower bounds

  using Density = TruncatedIndependentNormals<Vector,Real>;
  Density density(m,sd,a);

  std::size_t steps = 100000;

  // sanity check for t-walk mean and standard deviation
  {
    enSample::Sampler<Density> sampler(density,2);
    sampler.set_twalk();
    sampler.add_hook<enSample::MeanHook>(false);
    sampler.simulate(steps);
    if (check_mean(sampler,m,sd,0.3,0.3,{0,1,3}))
      return 1;
  }

  // sanity check for Ensemble Sampler mean and standard deviation
  {
    enSample::Sampler<Density> sampler(density,20);
    sampler.set_goodman_weare();
    sampler.set_target_active_components(2);
    sampler.add_hook<enSample::MeanHook>(false);
    sampler.simulate(steps);
    if (check_mean(sampler,m,sd,0.25,0.25,{0,1,3}))
      return 1;
  }

  // sanity check for DE-MCMC mean and standard deviation
  {
    enSample::Sampler<Density> sampler(density,30);
    sampler.set_differential_evolution();
    sampler.set_target_active_components(2);
    sampler.add_hook<enSample::MeanHook>(false);
    sampler.simulate(steps);
    if (check_mean(sampler,m,sd,0.25,0.25,{0,1,3}))
      return 1;
  }

  return 0;
}

