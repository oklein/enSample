#include "../src/ensample.hh"

#include "mockdensity.hh"

int main()
{
  using Density = MockDensity<enSample::Vector,double>;
  Density density;

  enSample::Sampler<Density> sampler(density,1);

  // test cyclic / non-cyclic toggle
  if (sampler.cyclic_chains())
    return 1;
  sampler.set_cyclic_chains(true);
  if (not sampler.cyclic_chains())
    return 1;
  sampler.set_cyclic_chains(false);
  if (sampler.cyclic_chains())
    return 1;

  // test setting number of active components
  if (sampler.target_active_components()
      != std::numeric_limits<double>::max())
    return 1;
  sampler.set_target_active_components(1.5);
  if (sampler.target_active_components() != 1.5)
    return 1;

  // test fixed active ratio toggle
  if (sampler.fixed_active_ratio())
    return 1;
  sampler.set_fixed_active_ratio(true);
  if (not sampler.fixed_active_ratio())
    return 1;
  sampler.set_fixed_active_ratio(false);
  if (sampler.fixed_active_ratio())
    return 1;

  return 0;
}

