#include "../examples/truncatednormal.hh"

int main()
{
  std::size_t seed = time(nullptr);
  enSample::rng<double>.seed(seed);

  // define underlying data type
  using enSample::Vector;
  using Real = double;

  // multivariate truncated Gaussian as per original implementation
  Vector<Real> m  = {2.0, 3.9, 5.2, 1.0, 7.8}; // mean
  Vector<Real> sd = {1.2, 2.1, 3.3, 0.1, 2.3}; // std dev
  Vector<Real> a  = {0.0, 0.0, 2.0, 0.0, 3.0}; // lower bounds

  using Density = TruncatedIndependentNormals<Vector,Real>;
  Density density(m,sd,a);

  std::size_t steps = 100000;

  // sanity check for t-walk acceptance ratio
  {
    enSample::Sampler<Density> sampler(density,2);
    sampler.set_twalk();
    sampler.simulate(steps);
    const auto& stats = sampler.provide_statistics();
    std::size_t acc_steps = stats[0][1];
    double acc_ratio = acc_steps / double(steps);
    std::cout << "acceptance ratio: " << acc_ratio << std::endl;
  
    // detect deviation from observed acceptance ratio 0.23
    if (acc_ratio < 0.18 || acc_ratio > 0.28)
      return 1;
  }

  // sanity check for Ensemble Sampler acceptance ratio
  {
    enSample::Sampler<Density> sampler(density,20);
    sampler.set_goodman_weare();
    sampler.set_target_active_components(2);
    sampler.simulate(steps);
    const auto& stats = sampler.provide_statistics();
    std::size_t acc_steps = stats[0][1];
    double acc_ratio = acc_steps / double(steps);
    std::cout << "acceptance ratio: " << acc_ratio << std::endl;
  
    // detect deviation from observed acceptance ratio 0.68
    if (acc_ratio < 0.63 || acc_ratio > 0.73)
      return 1;
  }

  // sanity check for DE-MCMC acceptance ratio
  {
    enSample::Sampler<Density> sampler(density,30);
    sampler.set_differential_evolution();
    sampler.set_target_active_components(2);
    sampler.simulate(steps);
    const auto& stats = sampler.provide_statistics();
    std::size_t acc_steps = stats[0][1];
    double acc_ratio = acc_steps / double(steps);
    std::cout << "acceptance ratio: " << acc_ratio << std::endl;
  
    // detect deviation from observed acceptance ratio 0.34
    if (acc_ratio < 0.29 || acc_ratio > 0.39)
      return 1;
  }

  return 0;
}

