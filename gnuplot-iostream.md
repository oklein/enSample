Instructions
============

enSample can use gnuplot for real-time sample visualization,
and provides examples that make use of this. However, this is
based on gnuplot-iostream [1], which is under the GPL and
therefore can't be distributed with this BSD-licensed package.

You have three different options to get this package and make
enSample pick it up:

* Some distros provide this in their package managers, in this
    case it should be picked up automatically if you have it
    installed. You can also install it manually and point to
    that location using `CMAKE_PREFIX_PATH` if needed.
* The only file that is actually needed is `gnuplot-iostream.h`.
    Copy this file into the `gnuplot-iostream` folder, and
    enSample should pick it up.
* The file is in the root directory of the gnuplot-iostream
    project, so it should be enough to clone that repository here:

    ```
    # in root folder of enSample project:
    git clone https://github.com/dstahlke/gnuplot-iostream
    ```

gnuplot-iostream needs Boost (its iostreams, system, and
filesystem components, to be precise) and gnuplot. These are
usually provided through package managers. Once you have all
three packages installed, turn on the `USE_GNUPLOT_VIZ` config
option.


[1] https://github.com/dstahlke/gnuplot-iostream
