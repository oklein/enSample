# - Try to find gnuplot-iostream
# Once done this will define
#  gnuplot-iostream_FOUND - System has gnuplot-iostream
#  gnuplot-iostream_INCLUDE_DIRS - The gnuplot-iostream include directories

find_package(PkgConfig)
pkg_check_modules(PC_gnuplot-iostream QUIET gnuplot-iostream)

find_path(gnuplot-iostream_INCLUDE_DIR gnuplot-iostream.h
          HINTS ${PC_gnuplot-iostream_INCLUDEDIR}
          ${PC_gnuplot-iostream_INCLUDE_DIRS}
          ${CMAKE_SOURCE_DIR}/gnuplot-iostream)

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set gnuplot-iostream_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(gnuplot-iostream
  DEFAULT_MSG gnuplot-iostream_INCLUDE_DIR)

mark_as_advanced(gnuplot-iostream_INCLUDE_DIR)

set(gnuplot-iostream_INCLUDE_DIRS ${gnuplot-iostream_INCLUDE_DIR})

message("gnuplot-iostream_FOUND ${gnuplot-iostream_FOUND}")
message("gnuplot-iostream_INCLUDE_DIR ${gnuplot-iostream_INCLUDE_DIR}")
message("gnuplot-iostream_INCLUDE_DIRS ${gnuplot-iostream_INCLUDE_DIRS}")
